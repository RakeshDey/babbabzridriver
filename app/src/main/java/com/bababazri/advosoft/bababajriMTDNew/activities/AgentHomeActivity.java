package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MapsFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.util.BadgeDrawable;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class AgentHomeActivity extends BaseActiivty
        implements NavigationView.OnNavigationItemSelectedListener, WebCompleteTask {
    LayerDrawable icon;
    ImageView profile;
    TextView name_txt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        profile = (ImageView)header. findViewById(R.id.imageView);
        name_txt=(TextView)header.findViewById(R.id.name);
        if (MyApplication.getProfileImage(AgentHomeActivity.this, "").isEmpty() == false) {
            Utils.loadImageRound(AgentHomeActivity.this, profile, WebUrls.BASE_URL + MyApplication.getProfileImage(AgentHomeActivity.this, ""));
        }
        name_txt.setText(MyApplication.getUserName(Constants.UserName));
        replaceFragment(R.id.container, new MapsFragment());
        callapiBadge();
        callsubtypeObj();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.agent_home, menu);
        MenuItem itemCart = menu.findItem(R.id.action_noti);
        icon = (LayerDrawable) itemCart.getIcon();
        setBadgeCount(this, icon, "0");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_noti) {
            startActivity(new Intent(AgentHomeActivity.this, NotificationListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_profile) {
            // Handle the camera action
            startActivity(new Intent(AgentHomeActivity.this, ProfileActivity.class));
        } else if (id == R.id.verification_list) {
            startActivity(new Intent(AgentHomeActivity.this, VerificationListActivity.class));
        } else if (id == R.id.verified_list) {
            startActivity(new Intent(AgentHomeActivity.this, VerifiedListActivity.class));

        } else if (id == R.id._term) {
            startActivity(new Intent(AgentHomeActivity.this, TermsCondition.class));
        } else if (id == R.id.emergency) {
            startActivity(new Intent(AgentHomeActivity.this, EmergencyActivity.class));
        } else if (id == R.id.logout) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AgentHomeActivity.this);
            alertDialogBuilder.setMessage(R.string.sure_logout);
            alertDialogBuilder.setPositiveButton(R.string.yes,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.cancel();
                            callapiAll();
                        }
                    });
            alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });


            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        //  objectNew.put("userid", MyApplication.getUserID(Constants.UserID));
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(AgentHomeActivity.this, WebUrls.BASE_URL + WebUrls.Api_Logout, objectNew, AgentHomeActivity.this, RequestCode.CODE_Logout, 1);
    }

    private void callapiBadge() {
        HashMap objectNew = new HashMap();
        //  objectNew.put("userid", MyApplication.getUserID(Constants.UserID));
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(AgentHomeActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetBadge, objectNew, AgentHomeActivity.this, RequestCode.CODE_GetBadge, 0);
    }

    @Override
    protected void onRestart() {
        if (MyApplication.getProfileImage(AgentHomeActivity.this, "").isEmpty() == false) {
            Utils.loadImageRound(AgentHomeActivity.this, profile, WebUrls.BASE_URL + MyApplication.getProfileImage(AgentHomeActivity.this, ""));
        }
        name_txt.setText(MyApplication.getUserName(Constants.UserName));
        if (MyApplication.getIslagChange(AgentHomeActivity.this)) {
            if (MyApplication.getlangID(Constants.LangId).compareTo("") != 0) {
                setLangRecreate(MyApplication.getlangID(Constants.LangId));
            } else {
                setLangRecreate("en");
            }
        } else {
            callapiBadge();
        }
        super.onRestart();
    }

    private void callsubtypeObj() {
        HashMap objectNew = new HashMap();

        new WebTask(AgentHomeActivity.this, WebUrls.BASE_URL + WebUrls.Api_SubtypeObj, objectNew, AgentHomeActivity.this, RequestCode.CODE_GetSubobj, 0);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_Logout) {
            MyApplication.setUserLoginStatus(AgentHomeActivity.this, false);
            startActivity(new Intent(AgentHomeActivity.this, LoginActivity.class));
            finish();
        } else if (taskcode == RequestCode.CODE_GetBadge) {
            try {
                JSONObject object = new JSONObject(response);
                String badge = object.getJSONObject("success").getJSONObject("data").optInt("badgeCount") + "";
                setBadgeCount(this, icon, badge);

            } catch (Exception e) {

            }
        }else if (taskcode==RequestCode.CODE_GetSubobj){
            MyApplication.setValueData(Constants.ValueData,response);

        }
    }

    public void setLangRecreate(String langval) {
        Locale locale;
        Configuration config = getResources().getConfiguration();
        locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        MyApplication.setLangId(Constants.LangId, langval);
        if (MyApplication.getIslagChange(AgentHomeActivity.this)) {
            MyApplication.setIslagChange(AgentHomeActivity.this, false);
            startActivity(new Intent(AgentHomeActivity.this, AgentHomeActivity.class));
            finish();

        }

    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }
}
