package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.FirebaseDriver;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/14/2018.
 */

public class LoginActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.sp_login_as)
    Spinner spLoginAs;
    @Bind(R.id.mobile_no)
    EditText mobileNo;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.cb_forget)
    TextView cbForget;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    @Bind(R.id.text_signup)
    TextView textSignup;
    String realm_string = "";
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        //spLoginAs.setAdapter(new HintAdapter(LoginActivity.this, R.layout.item_spinner, getResources().getStringArray(R.array.user_type_login)));
        textSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
            }
        });

        cbForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgetActivity.class));
            }
        });

        spLoginAs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    realm_string = "";
                } else {
                    if (position == 1) {
                        realm_string = "owner";
                    } else if (position == 2) {
                        realm_string = "agent";
                    } else if (position == 3) {
                        realm_string = "driver";
                    }
                    //realm_string = spLoginAs.getSelectedItem().toString().toLowerCase();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSubmitTask();
            }
        });
    }

    private void callSubmitTask() {

        if (TextUtils.isEmpty(mobileNo.getText().toString())) {
            mobileNo.setError(getString(R.string.notempty));
            mobileNo.requestFocus();
        } else if (mobileNo.getText().toString().length() < 10) {
            Toast.makeText(LoginActivity.this, R.string.mobile_not_valid, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError(getString(R.string.notempty));
            password.requestFocus();
        }else if (password.getText().length()<6){
            password.setError(getString(R.string.password_shdnot_less));
            password.requestFocus();
        } else if (realm_string.compareTo("") == 0) {
            Toast.makeText(LoginActivity.this, R.string.select_login_type, Toast.LENGTH_SHORT).show();
            spLoginAs.requestFocus();
        } else {
            HashMap objectNew = new HashMap();
            objectNew.put("realm", realm_string);
            objectNew.put("mobile", mobileNo.getText().toString());
            objectNew.put("password", password.getText().toString());
            objectNew.put("firebaseToken", MyApplication.getUserData(Constants.GCM_ID));
            objectNew.put("ln", MyApplication.getlangID(Constants.LangId));
            new WebTask(LoginActivity.this, WebUrls.BASE_URL + WebUrls.Api_Login, objectNew, LoginActivity.this, RequestCode.CODE_Login, 1);
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_Login) {
            try {
                JSONObject object = new JSONObject(response);
                JSONObject object_success = object.getJSONObject("success");
                JSONObject object_data = object_success.getJSONObject("data");
                JSONObject object_user = object_data.getJSONObject("user");
                MyApplication.setUserLoginStatus(LoginActivity.this, true);
                MyApplication.setUserLoginType(Constants.LoginType, object_user.optString("realm"));
                MyApplication.setUserID(Constants.UserID, object_data.optString("userId"));
                MyApplication.setUserType(Constants.userType, object_data.optString("id"));
                MyApplication.setUserName(Constants.UserName, object_user.optString("fullName"));
                MyApplication.setUserMobile(Constants.UserMobile, object_user.optString("mobile"));
                MyApplication.setUserLocation(Constants.UserLocation, object_user.optString("address"));
                MyApplication.setProfileImage(LoginActivity.this, object_user.optString("profileImage"));
                if (object_user.optString("realm").compareTo("agent") == 0) {
                    startActivity(new Intent(LoginActivity.this, AgentHomeActivity.class));
                    finish();
                } else if (object_user.optString("realm").compareTo("driver") == 0) {
                    Long tsLong = System.currentTimeMillis() / 1000;
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Driver");
                    FirebaseDriver firebaseLocation = new FirebaseDriver("0.0", "0.0", tsLong);
                    mDatabase.child(MyApplication.getUserID(Constants.UserID)).setValue(firebaseLocation);

                    startActivity(new Intent(LoginActivity.this, DriverHome.class));
                    finish();
                } else {
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }


            } catch (Exception e) {

            }
        }

    }
}
