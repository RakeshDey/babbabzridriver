package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.VehicalListWrapper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/18/2018.
 */

public class VehicalDetailActivity extends BaseActiivty {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.vehical_type)
    EditText vehicalType;
    @Bind(R.id.edit_truck_no)
    EditText editTruckNo;
    @Bind(R.id.edit_reg_no)
    EditText editRegNo;
    @Bind(R.id.img_polution)
    ImageView imgPolution;
    @Bind(R.id.img_rc)
    ImageView imgRc;
    @Bind(R.id.img_insurance)
    ImageView imgInsurance;
    @Bind(R.id.img_other)
    ImageView imgOther;
    VehicalListWrapper feed;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehical_detail_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.vehical_detail));
        feed= (VehicalListWrapper) getIntent().getSerializableExtra("data");
        vehicalType.setText(feed.getVehicalType());
        editTruckNo.setText(feed.getVehicalNo());
        editRegNo.setText(feed.getRegNo());
        if (feed.getPollutionCard().compareTo("")!=0){
            Utils.loadImage(VehicalDetailActivity.this,imgPolution, WebUrls.BASE_URL+feed.getPollutionCard());
        }
        if (feed.getRC().compareTo("")!=0){
            Utils.loadImage(VehicalDetailActivity.this,imgRc, WebUrls.BASE_URL+feed.getRC());
        }
        if (feed.getInsurance().compareTo("")!=0){
            Utils.loadImage(VehicalDetailActivity.this,imgInsurance, WebUrls.BASE_URL+feed.getInsurance());
        }
        if (feed.getOther().compareTo("")!=0){
            Utils.loadImage(VehicalDetailActivity.this,imgOther, WebUrls.BASE_URL+feed.getOther());
        }

        imgPolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.showFullImage(VehicalDetailActivity.this,WebUrls.BASE_URL+feed.getRC());
            }
        });
        imgRc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.showFullImage(VehicalDetailActivity.this,WebUrls.BASE_URL+feed.getPollutionCard());
            }
        });
        imgInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.showFullImage(VehicalDetailActivity.this,WebUrls.BASE_URL+feed.getInsurance());
            }
        });
        imgOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.showFullImage(VehicalDetailActivity.this,WebUrls.BASE_URL+feed.getOther());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
