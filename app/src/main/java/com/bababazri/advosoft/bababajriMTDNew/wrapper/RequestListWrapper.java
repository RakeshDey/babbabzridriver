package com.bababazri.advosoft.bababajriMTDNew.wrapper;

import java.io.Serializable;

/**
 * Created by advosoft on 6/20/2018.
 */

public class RequestListWrapper implements Serializable {
    String orderQuantity;
    String unit;
    String id;
    String productId;
    String orderDate;
    String deliveryDate;
    String ownerId;
    String price;
    String bookingStatus;
    String src_address;
    String vehical_nmbr;
    String vehical_type;
    String cus_name;
    String cus_number;
    String cus_address;
    String driver_name;
    String driver_number;

    public String getAgent_feedback() {
        return agent_feedback;
    }

    public void setAgent_feedback(String agent_feedback) {
        this.agent_feedback = agent_feedback;
    }

    public String getAgent_feedbackstatus() {
        return agent_feedbackstatus;
    }

    public void setAgent_feedbackstatus(String agent_feedbackstatus) {
        this.agent_feedbackstatus = agent_feedbackstatus;
    }

    String agent_feedback;
    String agent_feedbackstatus;

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    String subType;

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    String loadType;

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_number() {
        return cus_number;
    }

    public void setCus_number(String cus_number) {
        this.cus_number = cus_number;
    }

    public String getCus_address() {
        return cus_address;
    }

    public void setCus_address(String cus_address) {
        this.cus_address = cus_address;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_number() {
        return driver_number;
    }

    public void setDriver_number(String driver_number) {
        this.driver_number = driver_number;
    }

    public String getSrc_location() {
        return src_location;
    }

    public void setSrc_location(String src_location) {
        this.src_location = src_location;
    }

    String src_location;

    public String getSrc_address() {
        return src_address;
    }

    public void setSrc_address(String src_address) {
        this.src_address = src_address;
    }

    public String getVehical_nmbr() {
        return vehical_nmbr;
    }

    public void setVehical_nmbr(String vehical_nmbr) {
        this.vehical_nmbr = vehical_nmbr;
    }

    public String getVehical_type() {
        return vehical_type;
    }

    public void setVehical_type(String vehical_type) {
        this.vehical_type = vehical_type;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProduct_cat() {
        return product_cat;
    }

    public void setProduct_cat(String product_cat) {
        this.product_cat = product_cat;
    }

    String product;
    String product_cat;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    String address;
    String latlng;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    String distance;

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }
}
