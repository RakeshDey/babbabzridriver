package com.bababazri.advosoft.bababajriMTDNew.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.activities.DriverListBookingActivity;
import com.bababazri.advosoft.bababajriMTDNew.activities.MainActivity;
import com.bababazri.advosoft.bababajriMTDNew.fragments.BaseFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.DriverListWrapper;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.FirebaseLocation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class DriverListBookingAdapter extends RecyclerView.Adapter<DriverListBookingAdapter.MyViewHolder> implements Filterable, WebCompleteTask {
    private List<DriverListWrapper> feedsListall;
    private List<DriverListWrapper> mArrayList;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    int position_select = 0;
    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;
    String req_id, driver_id;

    public DriverListBookingAdapter(Context context, List<DriverListWrapper> feedsListall, String req_id) {
        this.context = context;
        this.feedsListall = feedsListall;
        this.mArrayList = feedsListall;
        this.req_id = req_id;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
    }

    public void setList(List<DriverListWrapper> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.driver_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    feedsListall = mArrayList;
                } else {

                    ArrayList<DriverListWrapper> filteredList = new ArrayList<>();

                    for (DriverListWrapper androidVersion : mArrayList) {
                        if (androidVersion.getName().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        } else if (androidVersion.getNumber().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    feedsListall = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = feedsListall;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                feedsListall = (ArrayList<DriverListWrapper>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DriverListWrapper feeds = feedsListall.get(position);

        holder.name.setText(feeds.getName());
        holder.number.setText(feeds.getNumber());
        holder.address.setText(feeds.getAddress());

        if (feeds.getProfile().compareTo("") != 0) {
            Utils.loadImageRound(context, holder.img, WebUrls.BASE_URL + feeds.getProfile());
        }

        holder.btn_assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiApply(feeds.getId());
                //  context.startActivity(new Intent(context, VehicalAssignActivity.class).putExtra("id", feeds.getId()).putExtra("data", feeds));


            }
        });

        if (feeds.getBlock()) {
            holder.btn_block.setText(R.string.unblock);
        } else {
            holder.btn_block.setText(R.string.block);
        }
        holder.btn_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiBlock(feeds.getId());
            }
        });


    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return feedsListall.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView name, number, address;
        private Button btn_assign,btn_block;


        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.profile);
            name = (TextView) itemView.findViewById(R.id.name);
            number = (TextView) itemView.findViewById(R.id.phone_nmbr);
            address = (TextView) itemView.findViewById(R.id.address);
            btn_assign = (Button) itemView.findViewById(R.id.btn_assign);
            btn_block = (Button) itemView.findViewById(R.id.btn_block);


        }
    }

    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + "day ago ";
            } else {
                time = diffDays + "days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + "hr ago";
                } else {
                    time = diffHours + "hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + "min ago";
                    } else {
                        time = diffMinutes + "mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + "secs ago";
                    }
                }

            }

        }
        return time;
    }

    private void callApiApply(String driverid) {
        driver_id = driverid;
        HashMap object = new HashMap();
        object.put("requestId", req_id);
        object.put("driverId", driverid);
        object.put("status", "Confirm");
        object.put("ln", MyApplication.getlangID(Constants.LangId));

        new WebTask(context, WebUrls.BASE_URL + WebUrls.Api_BookingCnfrm, object, DriverListBookingAdapter.this, RequestCode.CODE_VehicalAssign, 1);
    }

    private void callApiBlock(String driverid) {
        HashMap object = new HashMap();
        object.put("peopleId", driverid);
        object.put("ln", MyApplication.getlangID(Constants.LangId));


        new WebTask(context, WebUrls.BASE_URL + WebUrls.Api_DriverBlock, object, DriverListBookingAdapter.this, RequestCode.CODE_GetBlock, 1);
    }


    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_VehicalAssign) {
            Long tsLong = System.currentTimeMillis() / 1000;
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations");
            FirebaseLocation firebaseLocation = new FirebaseLocation(req_id, 0.0, 0.0, "pending", driver_id, tsLong);
            mDatabase.child(req_id).setValue(firebaseLocation);

            Toast.makeText(context, R.string.booking_confirm_succ, Toast.LENGTH_SHORT).show();
            context.startActivity(new Intent(context, MainActivity.class));
            ((DriverListBookingActivity) context).finish();

        }else if (taskcode==RequestCode.CODE_GetBlock){
            ((DriverListBookingActivity) context).Noti();
        }
    }
}
