package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.DriverListWrapper;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/18/2018.
 */

public class DriverDetailActivity extends BaseActiivty {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.edit_name)
    EditText editName;
    @Bind(R.id.edit_last_name)
    EditText editLastName;
    @Bind(R.id.edit_mobile)
    EditText editMobile;
    @Bind(R.id.address)
    PlacesAutocompleteTextView address;
    @Bind(R.id.img_licence)
    ImageView imgLicence;
    @Bind(R.id.ll_log)
    LinearLayout llLog;
    @Bind(R.id.img_aadhar_card)
    ImageView imgAadharCard;
    @Bind(R.id.ll_insurance)
    LinearLayout llInsurance;
    @Bind(R.id.ll_image)
    LinearLayout llImage;
    @Bind(R.id.img_photo)
    ImageView imgPhoto;
    @Bind(R.id.img_police)
    ImageView imgPolice;
    DriverListWrapper feeds;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_detail_new);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.driver_detail));
        feeds = (DriverListWrapper) getIntent().getSerializableExtra("data");
        editName.setText(feeds.getName());
        editLastName.setText(feeds.getLastName());
        editMobile.setText(feeds.getNumber());
        address.setText(feeds.getAddress());
        editName.setEnabled(false);
        editLastName.setEnabled(false);
        editMobile.setEnabled(false);
        address.setEnabled(false);
        if (feeds.getDriverLicense().compareTo("") != 0) {
            Utils.loadImage(DriverDetailActivity.this, imgLicence, WebUrls.BASE_URL + feeds.getDriverLicense());
        }
        if (feeds.getAadharCard().compareTo("") != 0) {
            Utils.loadImage(DriverDetailActivity.this, imgAadharCard, WebUrls.BASE_URL + feeds.getAadharCard());
        }
        if (feeds.getImage().compareTo("") != 0) {
            Utils.loadImage(DriverDetailActivity.this, imgPhoto, WebUrls.BASE_URL + feeds.getImage());
        }
        if (feeds.getPoliceVerify().compareTo("") != 0) {
            Utils.loadImage(DriverDetailActivity.this, imgPolice, WebUrls.BASE_URL + feeds.getPoliceVerify());
        }
        imgLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feeds.getDriverLicense().compareTo("") != 0) {
                    MyApplication.showFullImage(DriverDetailActivity.this, WebUrls.BASE_URL + feeds.getDriverLicense());
                }

            }
        });
        imgAadharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feeds.getAadharCard().compareTo("") != 0) {
                    MyApplication.showFullImage(DriverDetailActivity.this, WebUrls.BASE_URL + feeds.getAadharCard());
                }

            }
        });
        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feeds.getImage().compareTo("") != 0) {
                    MyApplication.showFullImage(DriverDetailActivity.this, WebUrls.BASE_URL + feeds.getImage());
                }

            }
        });
        imgPolice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feeds.getPoliceVerify().compareTo("") != 0) {
                    MyApplication.showFullImage(DriverDetailActivity.this, WebUrls.BASE_URL + feeds.getPoliceVerify());
                }

            }
        });
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
