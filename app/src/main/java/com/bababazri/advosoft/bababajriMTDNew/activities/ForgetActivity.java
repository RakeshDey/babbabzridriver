package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/15/2018.
 */

public class ForgetActivity extends BaseActiivty implements WebCompleteTask{
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.mobile_no)
    EditText mobileNo;
    @Bind(R.id.btn_submit)
    Button btnSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.forget_password);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobileNo.getText().toString().isEmpty()){
                    mobileNo.setError(getString(R.string.notempty));
                    mobileNo.requestFocus();
                }else if (mobileNo.getText().toString().length() < 10) {
                    Toast.makeText(ForgetActivity.this, R.string.mobile_not_valid, Toast.LENGTH_SHORT).show();
                }else {
                    HashMap objectNew = new HashMap();
                    objectNew.put("mobile", mobileNo.getText().toString());
                    objectNew.put("ln", MyApplication.getlangID(Constants.LangId));
                    new WebTask(ForgetActivity.this, WebUrls.BASE_URL + WebUrls.Api_Forget, objectNew, ForgetActivity.this, RequestCode.CODE_Login, 1);
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response",response);
        if (taskcode==RequestCode.CODE_Login){

            try {
                JSONObject object = new JSONObject(response);
                JSONObject innerObj = new JSONObject(object.getString("success"));
                JSONObject innerObjMsg = new JSONObject(innerObj.getString("msg"));
                String SuccessMsg = innerObjMsg.getString("replyMessage");
                JSONObject innerObj1 = new JSONObject(innerObj.getString("data"));
                JSONObject otp_obj = innerObj1.optJSONObject("passwordOtp");
                startActivity(new Intent(ForgetActivity.this, ForgetOtpActivity.class).putExtra("id", innerObj1.optString("id")).putExtra("otp", otp_obj.optInt("otp") + ""));
                finish();

            } catch (Exception e) {

            }

        }


    }
}
