package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.RequestListWrapper;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/5/2018.
 */

public class ApproveByAgentActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.edit_feedback)
    EditText editFeedback;
    @Bind(R.id.cb_verify)
    CheckBox cbVerify;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    RequestListWrapper feeds;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aproovebyagent);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.approve);
        feeds = (RequestListWrapper) getIntent().getSerializableExtra("data");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            ;
        }
        return super.onOptionsItemSelected(item);

    }

    private void callApi() {
        HashMap objectNew = new HashMap();
        objectNew.put("requestId", feeds.getId());
        objectNew.put("status", cbVerify.isChecked() + "");
        objectNew.put("feedback", editFeedback.getText().toString());

        new WebTask(ApproveByAgentActivity.this, WebUrls.BASE_URL + WebUrls.Api_VerificationByAgent, objectNew, ApproveByAgentActivity.this, RequestCode.CODE_Register, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        Toast.makeText(ApproveByAgentActivity.this, R.string.verification_complt, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(ApproveByAgentActivity.this, AgentHomeActivity.class));
        finish();
    }
}
