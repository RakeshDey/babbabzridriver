package com.bababazri.advosoft.bababajriMTDNew.fragments;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.adapter.DriverRequestListAdapter;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.DriverRequestWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;

/**
 * Created by Advosoft2 on 11/1/2017.
 */

public class DriverRequestFragment extends BaseFragment implements WebCompleteTask {
    static RecyclerView recyclerViewall;
    static SwipeRefreshLayout swipeRefreshLayout;
    static TextView emptyView;
    private static final int VERTICAL_ITEM_SPACE = 5;
    static ArrayList<DriverRequestWrapper> feedsListall = new ArrayList();
    static DriverRequestListAdapter adapterall;
    String profileimg;

    public DriverRequestFragment() {
    }

    public static DriverRequestFragment getInstance(Bundle bundle) {
        DriverRequestFragment chef = new DriverRequestFragment();
        chef.setArguments(bundle);
        return chef;
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    protected void initUi(View view) {
        recyclerViewall = (RecyclerView) view.findViewById(R.id.recycleView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        emptyView = (TextView) view.findViewById(R.id.empty_view);
        recyclerViewall.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewall.setLayoutManager(llm);
        recyclerViewall.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;
        callapiAll();
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onClick(View v) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.coman_layout_2, container, false);
        ButterKnife.bind(this, view);
        //disableBackButton();
        disbleBackButtonIcon();
        getActivity().setTitle(getString(R.string.new_request));
        initUi(view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        isRefreshing = true;
        callapiAll();
    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(getActivity(), WebUrls.BASE_URL + WebUrls.Api_DriverRequestList, objectNew, DriverRequestFragment.this, RequestCode.CODE_MyVehical, 0);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        swipeRefreshLayout.setRefreshing(false);
        try {
            Log.e("response", response);
            if (taskcode == RequestCode.CODE_MyVehical) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    JSONArray data = null;
                    data = object.getJSONObject("success").getJSONArray("data");
                    JSONObject jobj = null;
                    feedsListall.clear();
                    for (int i = 0; i < data.length(); i++) {
                        DriverRequestWrapper contract = new DriverRequestWrapper();
                        try {
                            jobj = data.getJSONObject(i);
                            contract.setId(jobj.getString("id"));
                            contract.setBookingStatus(jobj.optString("bookingStatus"));
                            contract.setDeliveryStatus(jobj.optString("deliveryStatus"));
                            contract.setDeliveryDate(jobj.optString("deliveryDate"));
                            contract.setOrderDate(jobj.optString("orderDate"));
                            contract.setOrderQuantity(jobj.optString("orderQuantity"));
                            contract.setUnit(jobj.optString("unit"));
                            contract.setPrice(jobj.optInt("price") + "");
                            contract.setOwnerId(jobj.optString("ownerId"));
                            contract.setProductId(jobj.optString("productId"));
                            contract.setDistance(jobj.optString("deliveryDistance"));
                            contract.setCustomerAddress(jobj.optJSONObject("customerAddress").optString("formattedAddress"));
                            contract.setCustomerLoaction(jobj.optJSONObject("customerAddress").optJSONObject("location") + "");
                            contract.setPickupaddress(jobj.optJSONObject("pickupAddress").optString("formattedAddress"));
                            contract.setPickupLocation(jobj.optJSONObject("pickupAddress").optJSONObject("location") + "");
                            contract.setMaterialname(jobj.optJSONObject("material").optString("name"));
                            contract.setMaterialTypeName(jobj.optJSONObject("materialType").optString("name"));
                            contract.setCusName(jobj.optJSONObject("customer").optString("fullName"));
                            contract.setCusMob(jobj.optJSONObject("customer").optString("mobile"));
                            contract.setCusAddress(jobj.optJSONObject("customer").optJSONObject("address").optString("address"));

                            feedsListall.add(contract);
                            emptyView.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    setList();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void setList() {
        System.out.println("service_list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new DriverRequestListAdapter(getActivity(), feedsListall);
            recyclerViewall.setAdapter(adapterall);


        }
    }
   /* @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search_food_new, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            case R.id.addProduct:
                startActivity(new Intent(getActivity(), NotificationActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapterall.getFilter().filter(newText);
                return true;
            }
        });
    }



    public void searchsubmit(String text) {
        adapterall.getFilter().filter(text);
    }
}
