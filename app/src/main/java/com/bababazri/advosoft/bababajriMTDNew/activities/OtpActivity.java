package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.chaos.view.PinView;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/15/2018.
 */

public class OtpActivity extends BaseActiivty implements WebCompleteTask {
    /* @Bind(R.id.edit_char_1)
     EditText editChar1;
     @Bind(R.id.edit_char_2)
     EditText editChar2;
     @Bind(R.id.edit_char_3)
     EditText editChar3;
     @Bind(R.id.edit_char_4)
     EditText editChar4;*/
    @Bind(R.id.btn_resend)
    Button btnResend;
    @Bind(R.id.btn_verify)
    Button btnVerify;
    String id;
    String otp;
    //SmsVerifyCatcher smsVerifyCatcher;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.pinView)
    PinView pinView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        id = getIntent().getStringExtra("id");
        otp = getIntent().getStringExtra("otp");

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinView.getText().toString().isEmpty() ) {
                    Toast.makeText(OtpActivity.this, R.string.pls_enter_otp, Toast.LENGTH_SHORT).show();
                } else {
                    callVerify();
                }

            }
        });
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callResend();
            }
        });

       /* smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                // etCode.setText(code);//set code in edit text
                //then you can send verification code to server

                editChar1.setText(code.charAt(0) + "");
                editChar2.setText(code.charAt(1) + "");
                editChar3.setText(code.charAt(2) + "");
                editChar4.setText(code.charAt(3) + "");
            }
        });*/
    }

  /*  private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }*/

    @Override
    public void onStart() {
        super.onStart();
        // smsVerifyCatcher.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        //smsVerifyCatcher.onStop();
    }

    private void callResend() {
        HashMap objectNew = new HashMap();
        objectNew.put("peopleId", id);
        objectNew.put("type", "signup");
        objectNew.put("ln", MyApplication.getlangID(Constants.LangId));

        new WebTask(OtpActivity.this, WebUrls.BASE_URL + WebUrls.Api_Resendotp, objectNew, OtpActivity.this, RequestCode.CODE_Resend, 1);
    }

    private void callVerify() {
        HashMap objectNew = new HashMap();
        objectNew.put("peopleId", id);
        objectNew.put("otp", pinView.getText().toString());
        objectNew.put("firebaseToken", "");
        objectNew.put("ln", MyApplication.getlangID(Constants.LangId));

        new WebTask(OtpActivity.this, WebUrls.BASE_URL + WebUrls.Api_VerifyOtp, objectNew, OtpActivity.this, RequestCode.CODE_VerifyOtp, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_Resend) {
            try {
                JSONObject object = new JSONObject(response);
                JSONObject innerObj = new JSONObject(object.getString("success"));
                JSONObject innerObjMsg = new JSONObject(innerObj.getString("msg"));
                String SuccessMsg = innerObjMsg.getString("replyMessage");
                JSONObject innerObj1 = new JSONObject(innerObj.getString("data"));
                JSONObject otp_obj = innerObj1.optJSONObject("signupOtp");
                String otp_resend = otp_obj.optInt("otp") + "";
               /* editChar1.setText(otp_resend.charAt(0) + "");
                editChar2.setText(otp_resend.charAt(1) + "");
                editChar3.setText(otp_resend.charAt(2) + "");
                editChar4.setText(otp_resend.charAt(3) + "");
                otp = otp_resend;*/

            } catch (Exception e) {

            }

        } else if (taskcode == RequestCode.CODE_VerifyOtp) {
            Toast.makeText(OtpActivity.this, R.string.registration_successful, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(OtpActivity.this, LoginActivity.class));
            finish();

        }

    }

  /*  @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }*/
}
