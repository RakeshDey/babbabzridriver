package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;

import java.util.Locale;


/**
 * Created by Advosoft2 on 12/2/2017.
 */

public class SplashActivity extends Activity {
    private static String TAG = "Splash";
    private Boolean mLocationPermissionsGranted = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (MyApplication.getlangID(Constants.LangId).compareTo("")!=0){
            setLangRecreate(MyApplication.getlangID(Constants.LangId));
        }else {
           /* if (Locale.getDefault().getLanguage().compareTo("hi")==0){
                setLangRecreate("hi");
            }else {
                setLangRecreate("en");
            }*/

        }

        if (getLocationPermission()){
            initApp();
        }

    }

    private boolean getLocationPermission() {

        String[] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.CAMERA,android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE

                };

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {


                ActivityCompat.requestPermissions(this, permissions, RequestCode.LOCATION_PERMISSION_REQUEST_CODE);
                // Permission Denied
                Toast.makeText(SplashActivity.this, R.string.some_permission, Toast.LENGTH_SHORT)
                        .show();
            }
            else
            {
                ActivityCompat.requestPermissions(this, permissions, RequestCode.LOCATION_PERMISSION_REQUEST_CODE);

            }
        }
        else
        {
            mLocationPermissionsGranted = true;
            Log.d(TAG, "getLocationPermission: mLocationPermissionsGranted = true");
        }
        return mLocationPermissionsGranted;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case RequestCode.LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            finish();
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    initApp();
                    //initialize our map
                }
            }
        }
    }

    private void initApp(){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (MyApplication.getUserLoginStatus(getApplicationContext())) {
                    if (MyApplication.getUserLoginType(Constants.LoginType).compareTo("agent")==0){
                        intent = new Intent(SplashActivity.this, AgentHomeActivity.class);
                        // intent = new Intent(SplashActivity.this, DriverHome.class);
                       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }else if (MyApplication.getUserLoginType(Constants.LoginType).compareTo("driver")==0){
                        intent = new Intent(SplashActivity.this, DriverHome.class);
                        // intent = new Intent(SplashActivity.this, DriverHome.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }else {
                        intent = new Intent(SplashActivity.this, MainActivity.class);
                        // intent = new Intent(SplashActivity.this, DriverHome.class);
                       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    intent = new Intent(SplashActivity.this, SelectLanguage.class);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        }, 1500);
    }

    public void setLangRecreate(String langval) {
        Locale locale;
        Configuration config = getResources().getConfiguration();
        locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        MyApplication.setLangId(Constants.LangId, langval);
        //   MyApplication.setIslagChange(ProfileActivity.this, true);
       // recreate();

    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
       // recreate();

    }

}
