package com.bababazri.advosoft.bababajriMTDNew.wrapper;

import java.io.Serializable;

/**
 * Created by advosoft on 7/12/2018.
 */

public class NotificationWapper implements Serializable {
    String title;
    String body;
    String type;
    String sendTo;
    String orderRequestId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getOrderRequestId() {
        return orderRequestId;
    }

    public void setOrderRequestId(String orderRequestId) {
        this.orderRequestId = orderRequestId;
    }
}
