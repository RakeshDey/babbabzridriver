package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.ApiConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.AppConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.google.gson.JsonObject;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by advosoft on 7/4/2018.
 */

public class ProfileActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.profile_img)
    ImageView profileImg;
    @Bind(R.id.img_edit)
    ImageView imgEdit;
    @Bind(R.id.frist_name)
    EditText fristName;
    @Bind(R.id.last_name)
    EditText lastName;
    @Bind(R.id.mobile_no)
    EditText mobileNo;
    @Bind(R.id.address)
    PlacesAutocompleteTextView address;
    @Bind(R.id.change_password)
    TextView changePassword;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    String filename = "";
    PlaceDetails addressplace;
    JSONObject addressJson = null;
    String addressId;
    @Bind(R.id.change_language)
    TextView changeLanguage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.profile));
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        callapiAll();
        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageTypePost();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fristName.getText().toString().isEmpty()) {
                    fristName.setError(getString(R.string.notempty));
                    fristName.requestFocus();
                } else if (lastName.getText().toString().isEmpty()) {
                    lastName.setError(getString(R.string.notempty));
                    lastName.requestFocus();
                } else if (address.getText().toString().isEmpty()) {
                    address.setError(getString(R.string.notempty));
                    address.requestFocus();
                } else {
                    validateViews();
                }

            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, ChangePasswordActivity.class));
            }
        });
        changeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        address.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull final Place place) {
                addressId = place.place_id;
//                MyApplication.showProgressDialog("Getting Location");
                address.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                        addressplace = placeDetails;

                        addressJson = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            addressJson.put("address", placeDetails.formatted_address);
                            addressJson.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(ProfileActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetUserProfile, objectNew, ProfileActivity.this, RequestCode.CODE_MyVehical, 0);
    }

    private void callapichangelang(String ln) {
        HashMap objectNew = new HashMap();
        objectNew.put("ln", ln);
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(ProfileActivity.this, WebUrls.BASE_URL + WebUrls.Api_ChangeLng, objectNew, ProfileActivity.this, RequestCode.CODE_ChangeLng, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_MyVehical) {
            try {
                JSONObject object = new JSONObject(response);
                JSONObject data_obj = object.getJSONObject("success").getJSONObject("data");
                fristName.setText(data_obj.optString("firstName"));
                lastName.setText(data_obj.optString("lastName"));
                address.setText(data_obj.optJSONObject("address").optString("address"));
                mobileNo.setText(data_obj.optString("mobile"));
                if (data_obj.optString("profileImage") != null) {
                    Utils.loadImageRound(ProfileActivity.this, profileImg, WebUrls.BASE_URL + data_obj.optString("profileImage"));
                }

            } catch (Exception e) {
                Log.d("exception...", e.toString());
            }
        }else if (taskcode==RequestCode.CODE_ChangeLng){
            setLangRecreate(MyApplication.getlangID(Constants.LangId));
        }

    }

    public void uploadImageTypePost() {
        setimagepath();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);

        // set title
        alertDialogBuilder.setTitle(R.string.choose_option);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.select_img_from)
                .setCancelable(false)
                .setPositiveButton(R.string.camra,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton(R.string.galary,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(ProfileActivity.this.getFilesDir(), fileName);
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filename = getPath(ProfileActivity.this, data.getData());
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    filename = getPath(this, getImageUri(this, resized));
                    Utils.loadImageRound(ProfileActivity.this, profileImg, "file://" + mFileTemp.getPath());

                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
                // Utils.loadImageRoundSignup(this, imageView, "file://" + mFileTemp.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                filename = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filename = mFileTemp.getAbsolutePath();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                filename = getPath(ProfileActivity.this, getImageUri(ProfileActivity.this, resized));
                Utils.loadImageRound(ProfileActivity.this, profileImg, "file://" + mFileTemp.getPath());

                //  Utils.loadImageRoundSignup(AddMarketPlace.this, imageView, "file://" + mFileTemp.getPath());


            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        }

    }

    private void validateViews() {
        if (Utils.isConnectingToInternet(ProfileActivity.this)) {
            try {
                enableLoadingBar(true);
                JSONObject obj = new JSONObject();
                obj.put("firstName", fristName.getText().toString());
                obj.put("lastName", lastName.getText().toString());
                obj.put("mobile", mobileNo.getText().toString());
                obj.put("address", addressJson);

                if (filename.compareTo("") != 0) {
                    File file = new File(filename);
                    RequestBody requestBody2 = RequestBody.create(MediaType.parse(getMimeType(filename)), file);
                    MultipartBody.Part part_licence = MultipartBody.Part.createFormData("profileImage", file.getName(), requestBody2);

                    System.out.println("update json " + obj.toString());
                    RequestBody data = RequestBody.create(MediaType.parse("text/plain"), obj.toString().trim());
                    ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
                    Log.d("auth", MyApplication.getUserType(Constants.userType));
                    Call<JsonObject> call = getResponse.UpdateProfile(part_licence, data, MyApplication.getUserType(Constants.userType),MyApplication.getlangID(Constants.LangId));
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            dismissProgressBar();
                            if (response.isSuccessful()) {
                                JSONObject obj = null;
                                try {
                                    obj = new JSONObject(response.body().toString());
                                    Log.d("response", obj.toString());
                                    MyApplication.setProfileImage(ProfileActivity.this, obj.optJSONObject("success").optJSONObject("data").optString("profileImage"));
                                    MyApplication.setUserName(Constants.UserName, obj.optJSONObject("success").optJSONObject("data").optString("fullName"));
                                    Toast.makeText(ProfileActivity.this, R.string.profile_update_success, Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //TestFragment.imageStringpath.clear();
                                dismissProgressBar();
                                try {
                                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                    System.out.println("error response " + jsonObject.toString());

                                } catch (Exception e) {

                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            dismissProgressBar();
                            //  TestFragment.imageStringpath.clear();
                            Toast.makeText(ProfileActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                            System.out.println("fail response.." + t.toString());
                            try {
                                JSONObject object = new JSONObject(t.toString());
                                Toast.makeText(ProfileActivity.this, object.getJSONObject("error").optString("message") + "", Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {

                            }
                            dismissProgressBar();
                        }
                    });
                } else {
                    System.out.println("update json " + obj.toString());
                    RequestBody data = RequestBody.create(MediaType.parse("text/plain"), obj.toString().trim());
                    ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
                    Log.d("auth", MyApplication.getUserType(Constants.userType));
                    Call<JsonObject> call = getResponse.UpdateProfileNew(data, MyApplication.getUserType(Constants.userType),MyApplication.getlangID(Constants.LangId));
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            dismissProgressBar();
                            if (response.isSuccessful()) {
                                JSONObject obj = null;
                                try {
                                    obj = new JSONObject(response.body().toString());
                                    Log.d("response", obj.toString());
                                    MyApplication.setUserName(Constants.UserName, obj.optJSONObject("success").optJSONObject("data").optString("fullName"));
                                    Toast.makeText(ProfileActivity.this, R.string.profile_update_success, Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //TestFragment.imageStringpath.clear();
                                dismissProgressBar();
                                try {
                                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                    System.out.println("error response " + jsonObject.toString());

                                } catch (Exception e) {

                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            dismissProgressBar();
                            //  TestFragment.imageStringpath.clear();
                            Toast.makeText(ProfileActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                            System.out.println("fail response.." + t.toString());
                            try {
                                JSONObject object = new JSONObject(t.toString());
                                Toast.makeText(ProfileActivity.this, object.getJSONObject("error").optString("message") + "", Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {

                            }
                            dismissProgressBar();
                        }
                    });
                }

            } catch (Exception e) {
                // TestFragment.imageStringpath.clear();
                System.out.println("Exception..." + e.toString());
                dismissProgressBar();
            }
        } else {
            Toast.makeText(ProfileActivity.this, getResources().getString(R.string.network_error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void showDialog() {
        //Dialog dialog = new Dialog(context, android.R.style.Theme_Light); --for full screen
        final Dialog dialog = new Dialog(ProfileActivity.this, R.style.MyDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.language_dialog);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radio_grp);
        RadioButton rb_eng = (RadioButton) dialog.findViewById(R.id.rb_english);
        RadioButton rb_hindi = (RadioButton) dialog.findViewById(R.id.rb_hindi);

        if (MyApplication.getlangID(Constants.LangId).compareTo("en") == 0) {
            rb_eng.setChecked(true);
        } else if (MyApplication.getlangID(Constants.LangId).compareTo("hi") == 0) {
            rb_hindi.setChecked(true);
        }  else {
            rb_eng.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                if (radioButtonID == R.id.rb_english) {
                    dialog.dismiss();
                    PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("LANG", "en").commit();
                  //  setLangRecreate("en");
                    MyApplication.setLangId(Constants.LangId, "en");
                    callapichangelang("en");

                }
                if (radioButtonID == R.id.rb_hindi) {
                    dialog.dismiss();
                    PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("LANG", "hi").commit();
                   // setLangRecreate("hi");
                    MyApplication.setLangId(Constants.LangId, "hi");
                    callapichangelang("hi");

                }



            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });


        dialog.show();

    }

    public void setLangRecreate(String langval) {
        Locale locale;
        Configuration config = getResources().getConfiguration();
        locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        MyApplication.setLangId(Constants.LangId, langval);
        MyApplication.setIslagChange(ProfileActivity.this, true);
        recreate();

    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        recreate();

    }

}
