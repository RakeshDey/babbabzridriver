package com.bababazri.advosoft.bababajriMTDNew.wrapper;

import java.io.Serializable;

/**
 * Created by advosoft on 6/20/2018.
 */

public class VehicalListWrapper implements Serializable {
    String id;
    String truckPic;
    String vehicalNo;
    String regNo;
    String vehicalType;
    String pollutionCard;
    String RC;
    String insurance;
    String other;

    public String getPollutionCard() {
        return pollutionCard;
    }

    public void setPollutionCard(String pollutionCard) {
        this.pollutionCard = pollutionCard;
    }

    public String getRC() {
        return RC;
    }

    public void setRC(String RC) {
        this.RC = RC;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(String ownerid) {
        this.ownerid = ownerid;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    String ownerid;
    String approveStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTruckPic() {
        return truckPic;
    }

    public void setTruckPic(String truckPic) {
        this.truckPic = truckPic;
    }

    public String getVehicalNo() {
        return vehicalNo;
    }

    public void setVehicalNo(String vehicalNo) {
        this.vehicalNo = vehicalNo;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getVehicalType() {
        return vehicalType;
    }

    public void setVehicalType(String vehicalType) {
        this.vehicalType = vehicalType;
    }
}
