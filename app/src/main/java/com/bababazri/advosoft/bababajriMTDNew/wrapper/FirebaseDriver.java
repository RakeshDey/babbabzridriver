package com.bababazri.advosoft.bababajriMTDNew.wrapper;

/**
 * Created by advosoft on 7/12/2018.
 */

public class FirebaseDriver {
    public FirebaseDriver(String lat, String lng, long timestamp) {
        this.lat = lat;
        this.lng = lng;
        this.timestamp = timestamp;
    }

    public String lat;

    public String lng;

    public long timestamp;
}
