package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.adapter.NotiListAdapter;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.NotificationWapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/20/2018.
 */

public class NotificationListActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.empty_view)
    TextView emptyView;

    private static final int VERTICAL_ITEM_SPACE = 15;
    ArrayList<NotificationWapper> feedsListall = new ArrayList();
    NotiListAdapter adapterall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coman_layout);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.notification));

        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();
        callapiRead();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;
        callapiAll();
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(NotificationListActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetNoti, objectNew, NotificationListActivity.this, RequestCode.CODE_MyVehical, 0);
    }
    private void callapiRead() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(NotificationListActivity.this, WebUrls.BASE_URL + WebUrls.Api_ReadNoti, objectNew, NotificationListActivity.this, RequestCode.CODE_ReadNoti, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("rtesponse", response);
        if (taskcode == RequestCode.CODE_MyVehical) {
            swipeRefreshLayout.setRefreshing(false);
            try {
                JSONObject object = new JSONObject(response);
                JSONArray data_array = object.getJSONObject("success").getJSONArray("data");
                JSONObject data_obj = null;
                feedsListall.clear();
                for (int i = 0; i < data_array.length(); i++) {
                    data_obj = data_array.getJSONObject(i);
                    NotificationWapper vehicalListWrapper = new NotificationWapper();
                    vehicalListWrapper.setTitle(data_obj.optString("title"));
                    vehicalListWrapper.setBody(data_obj.optString("body"));
                    vehicalListWrapper.setOrderRequestId(data_obj.optString("orderRequestId"));
                    vehicalListWrapper.setType(data_obj.optString("type"));
                    vehicalListWrapper.setSendTo(data_obj.optString("sendTo"));

                    feedsListall.add(vehicalListWrapper);
                }
                setList();
            } catch (Exception e) {

            }
        }

    }

    public void setList() {
        System.out.println("service_list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new NotiListAdapter(NotificationListActivity.this, feedsListall);
            recycleView.setAdapter(adapterall);


        }
    }
}
