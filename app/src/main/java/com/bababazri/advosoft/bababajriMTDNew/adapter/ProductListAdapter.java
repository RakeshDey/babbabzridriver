package com.bababazri.advosoft.bababajriMTDNew.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.activities.EditProduct;
import com.bababazri.advosoft.bababajriMTDNew.activities.ProductListActivity;
import com.bababazri.advosoft.bababajriMTDNew.fragments.BaseFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.ProductWrapper;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> implements Filterable, WebCompleteTask {
    private List<ProductWrapper> feedsListall;
    private List<ProductWrapper> mArrayList;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    int position_select = 0;
    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id;
    RequestQueue queue;
    ProgressDialog pd;
    JSONObject object_data;

    public ProductListAdapter(Context context, List<ProductWrapper> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        this.mArrayList = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
        try{
            object_data=new JSONObject(MyApplication.getValueData(Constants.ValueData)).getJSONObject("success").getJSONObject("data");
        }catch (Exception e){

        }
    }

    public void setList(List<ProductWrapper> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.product_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    feedsListall = mArrayList;
                } else {

                    ArrayList<ProductWrapper> filteredList = new ArrayList<>();

                    for (ProductWrapper androidVersion : mArrayList) {
                        if (androidVersion.getMaterialName().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        } else if (androidVersion.getMaterialSubcat().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    feedsListall = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = feedsListall;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                feedsListall = (ArrayList<ProductWrapper>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductWrapper feeds = feedsListall.get(position);

        holder.material_name.setText(feeds.getMaterialName());
        holder.material_sub.setText(feeds.getMaterialSubcat());
        holder.address.setText(feeds.getAddress());
        holder.price.setText(feeds.getPrice() + " "+context.getString(R.string.rupee_per) +" "+object_data.optString(feeds.getUnit().trim().toLowerCase()));
        holder.img_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiApply(feeds.getId());
            }
        });

        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context,EditProduct.class).putExtra("data",feeds));
            }
        });

        if (feeds.getProductStatus().compareTo("active")==0){
            holder.prod_status.setText(R.string.active);

        }else {
            holder.prod_status.setText(R.string.deactive);
        }


    }

    private void callApiApply(String pro_id) {
        HashMap object = new HashMap();
        object.put("productId", pro_id);


        new WebTask(context, WebUrls.BASE_URL + WebUrls.Api_Product_Delete, object, ProductListAdapter.this, RequestCode.CODE_VehicalAssign, 1);
    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return feedsListall.size();
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_VehicalAssign) {
            //notifyDataSetChanged();
            ((ProductListActivity) context).finish();
            context.startActivity(new Intent(context, ProductListActivity.class));

        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView material_name, material_sub, address, price,prod_status;
        private ImageView img_del,img_edit;


        public MyViewHolder(View itemView) {
            super(itemView);
            material_name = (TextView) itemView.findViewById(R.id.product_cat);
            material_sub = (TextView) itemView.findViewById(R.id.product_subcat);
            address = (TextView) itemView.findViewById(R.id.address);
            price = (TextView) itemView.findViewById(R.id.price_unit);
            img_del = (ImageView) itemView.findViewById(R.id.img_delete);
            img_edit=(ImageView)itemView.findViewById(R.id.img_edit);
            prod_status=(TextView)itemView.findViewById(R.id.prod_status);
        }
    }

    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + "day ago ";
            } else {
                time = diffDays + "days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + "hr ago";
                } else {
                    time = diffHours + "hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + "min ago";
                    } else {
                        time = diffMinutes + "mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + "secs ago";
                    }
                }

            }

        }
        return time;
    }

}
