package com.bababazri.advosoft.bababajriMTDNew.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.CurrentListFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.PastListFragment;

/**
 * Created by advosoft on 6/25/2018.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    Context ctx;

    public ViewPagerAdapter(Context ctx,FragmentManager fm) {
        super(fm);
        this.ctx=ctx;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new CurrentListFragment();
        }
        else if (position == 1)
        {
            fragment = new PastListFragment();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = ctx.getString(R.string.current_list);
        }
        else if (position == 1)
        {
            title = ctx.getString(R.string.past_list);
        }

        return title;
    }
}
