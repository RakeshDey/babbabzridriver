package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.ApiConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.AppConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by advosoft on 6/18/2018.
 */

public class AddTruckActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.sp_truck)
    Spinner spTruck;
    @Bind(R.id.edit_truck_no)
    EditText editTruckNo;
    @Bind(R.id.edit_reg_no)
    EditText editRegNo;
    @Bind(R.id.img_polution)
    ImageView imgPolution;
    @Bind(R.id.img_rc)
    ImageView imgRc;
    @Bind(R.id.img_insurance)
    ImageView imgInsurance;
    @Bind(R.id.img_other)
    ImageView imgOther;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    ArrayList<String> vehical_name = new ArrayList<>();
    ArrayList<String> vehical_id = new ArrayList<>();
    String filepath = null;
    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    int image_type = 0;
    String file_polution = "", file_rc = "", file_insurance = "", file_other = "";
    String vehical_id_str = "";
    @Bind(R.id.sp_tyre)
    Spinner spTyre;
    ArrayList<ArrayList<String>> data_tyre = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_truck);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.add_vehical);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        callgetVehicaltype();

        imgPolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 1;
                uploadImageTypePost();
            }
        });
        imgRc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 2;
                uploadImageTypePost();
            }
        });
        imgInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 3;
                uploadImageTypePost();
            }
        });
        imgOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 4;
                uploadImageTypePost();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTruckNo.getText().toString().isEmpty()) {
                    editTruckNo.setError(getString(R.string.notempty));
                    editTruckNo.setFocusable(true);
                } else if (editRegNo.getText().toString().isEmpty()) {
                    editRegNo.setError(getString(R.string.notempty));
                    editRegNo.setFocusable(true);
                } else if (file_polution.compareTo("") == 0) {
                    Toast.makeText(AddTruckActivity.this, R.string.pls_select_polution_img, Toast.LENGTH_SHORT).show();
                } else if (file_rc.compareTo("") == 0) {
                    Toast.makeText(AddTruckActivity.this, R.string.pls_select_rc_img, Toast.LENGTH_SHORT).show();
                } else if (file_insurance.compareTo("") == 0) {
                    Toast.makeText(AddTruckActivity.this, R.string.pls_select_insurance_img, Toast.LENGTH_SHORT).show();
                }/* else if (file_other.compareTo("") == 0) {
                    Toast.makeText(AddTruckActivity.this, "please select Other image", Toast.LENGTH_SHORT).show();
                }*/ else {
                    validateViews();
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callgetVehicaltype() {
        HashMap objectNew = new HashMap();

        new WebTask(AddTruckActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetVehicalType, objectNew, AddTruckActivity.this, RequestCode.CODE_VehicalType, 0);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_VehicalType) {
            try {
                JSONObject object = new JSONObject(response);
                JSONObject success_obj = object.getJSONObject("success");
                JSONArray jsonArray = success_obj.getJSONArray("data");
                JSONObject obj_content = null;
                vehical_name.clear();
                vehical_id.clear();
                data_tyre.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    obj_content = jsonArray.getJSONObject(i);
                    vehical_name.add(obj_content.optString("name"));
                    vehical_id.add(obj_content.optString("id"));
                    ArrayList<String> tyre = new ArrayList<>();
                    tyre.clear();
                    JSONArray obj_tyr = obj_content.getJSONArray("loadValue");
                    JSONObject objjb = null;
                    for (int j = 0; j < obj_tyr.length(); j++) {
                        objjb = obj_tyr.getJSONObject(j);
                        tyre.add(objjb.optInt("tyres") + "");
                    }
                    data_tyre.add(tyre);
                }
                setAdapter();
            } catch (Exception e) {

            }
        }

    }

    private void setAdapter() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, vehical_name);
        spTruck.setAdapter(adapter);

        spTruck.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                vehical_id_str = vehical_id.get(i);
                ArrayAdapter<String> adapter_tyre = new ArrayAdapter<String>(AddTruckActivity.this, android.R.layout.simple_spinner_dropdown_item, data_tyre.get(i));
                spTyre.setAdapter(adapter_tyre);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void uploadImageTypePost() {
        setimagepath();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(R.string.choose_option);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.select_img_from)
                .setCancelable(false)
                .setPositiveButton(R.string.camra,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton(R.string.galary,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(getFilesDir(), fileName);
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filepath = getPath(AddTruckActivity.this, data.getData());
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());

                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    filepath = getPath(this, getImageUri(this, resized));
                    if (image_type == 1) {
                        file_polution = getPath(this, getImageUri(this, resized));

                        Utils.loadImage(this, imgPolution, "file://" + mFileTemp.getPath());
                    } else if (image_type == 2) {
                        file_rc = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgRc, "file://" + mFileTemp.getPath());
                    } else if (image_type == 3) {
                        file_insurance = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgInsurance, "file://" + mFileTemp.getPath());
                    } else if (image_type == 4) {
                        file_other = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgOther, "file://" + mFileTemp.getPath());
                    }
                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
                // Utils.loadImageRoundSignup(this, imageView, "file://" + mFileTemp.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                filepath = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filepath = mFileTemp.getAbsolutePath();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);

                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                filepath = getPath(AddTruckActivity.this, getImageUri(AddTruckActivity.this, resized));
                if (image_type == 1) {
                    file_polution = getPath(AddTruckActivity.this, getImageUri(AddTruckActivity.this, resized));
                    Utils.loadImage(AddTruckActivity.this, imgPolution, "file://" + mFileTemp.getPath());
                } else if (image_type == 2) {
                    file_rc = getPath(AddTruckActivity.this, getImageUri(AddTruckActivity.this, resized));
                    Utils.loadImage(AddTruckActivity.this, imgRc, "file://" + mFileTemp.getPath());
                } else if (image_type == 3) {
                    file_insurance = getPath(AddTruckActivity.this, getImageUri(AddTruckActivity.this, resized));
                    Utils.loadImage(AddTruckActivity.this, imgInsurance, "file://" + mFileTemp.getPath());
                } else if (image_type == 4) {
                    file_other = getPath(AddTruckActivity.this, getImageUri(AddTruckActivity.this, resized));
                    Utils.loadImage(AddTruckActivity.this, imgOther, "file://" + mFileTemp.getPath());
                }
                //  Utils.loadImageRoundSignup(AddMarketPlace.this, imageView, "file://" + mFileTemp.getPath());


            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        }

    }

    private void validateViews() {
        if (Utils.isConnectingToInternet(AddTruckActivity.this)) {
            try {
                enableLoadingBar(true);
                JSONObject obj = new JSONObject();
                obj.put("vehicleTypeId", vehical_id_str);
                obj.put("truckNumber", editTruckNo.getText().toString());
                obj.put("registrationNo", editRegNo.getText().toString());
                obj.put("tyres", spTyre.getSelectedItem().toString());
                MultipartBody.Part part_licence = null, part_aadhar = null, part_photo = null, part_police = null;
                File file = null, file1 = null, file2 = null, file3 = null;
                if (file_polution.compareTo("") != 0) {
                    file = new File(file_polution);
                    RequestBody requestBody2 = RequestBody.create(MediaType.parse(getMimeType(file_polution)), file);
                    part_licence = MultipartBody.Part.createFormData("pollutionCard", file.getName(), requestBody2);
                }
                if (file_rc.compareTo("") != 0) {
                    file1 = new File(file_rc);
                    RequestBody requestBody21 = RequestBody.create(MediaType.parse(getMimeType(file_rc)), file1);
                    part_aadhar = MultipartBody.Part.createFormData("RC", file1.getName(), requestBody21);
                }

                if (file_insurance.compareTo("") != 0) {
                    file2 = new File(file_insurance);
                    RequestBody requestBody22 = RequestBody.create(MediaType.parse(getMimeType(file_insurance)), file2);
                    part_photo = MultipartBody.Part.createFormData("insurance", file2.getName(), requestBody22);
                }

                if (file_other.compareTo("") != 0) {
                    file3 = new File(file_other);
                    RequestBody requestBody23 = RequestBody.create(MediaType.parse(getMimeType(file_other)), file3);
                    part_police = MultipartBody.Part.createFormData("other", file3.getName(), requestBody23);
                }


                System.out.println("update json " + obj.toString());
                RequestBody data = RequestBody.create(MediaType.parse("text/plain"), obj.toString().trim());
                ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
                Log.d("auth", MyApplication.getUserType(Constants.userType));
                Call<JsonObject> call = getResponse.AddTruck(part_licence, part_aadhar, part_photo, part_police, data, MyApplication.getUserType(Constants.userType),MyApplication.getlangID(Constants.LangId));
                final File finalFile = file;
                final File finalFile1 = file1;
                final File finalFile2 = file2;
                final File finalFile3 = file3;
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        dismissProgressBar();
                        if (response.isSuccessful()) {
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response.body().toString());
                                Log.d("response", obj.toString());
                                if (finalFile != null) {
                                    finalFile.delete();
                                }
                                if (finalFile1 != null) {
                                    finalFile1.delete();
                                }
                                if (finalFile2 != null) {
                                    finalFile2.delete();
                                }
                                if (finalFile3 != null) {
                                    finalFile3.delete();
                                }
                                Toast.makeText(AddTruckActivity.this, R.string.vehical_added_succ, Toast.LENGTH_SHORT).show();
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //TestFragment.imageStringpath.clear();
                            dismissProgressBar();
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                System.out.println("error response " + jsonObject.getJSONObject("error").toString());
                                Toast.makeText(AddTruckActivity.this, jsonObject.getJSONObject("error").optString("message"), Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        dismissProgressBar();
                        //  TestFragment.imageStringpath.clear();
                        Toast.makeText(AddTruckActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                        System.out.println("fail response.." + t.toString());
                        try {
                            JSONObject object = new JSONObject(t.toString());
                            Toast.makeText(AddTruckActivity.this, object.getJSONObject("error").optString("message") + "", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {

                        }
                        dismissProgressBar();
                    }
                });
            } catch (Exception e) {
                // TestFragment.imageStringpath.clear();
                System.out.println("Exception..." + e.toString());
                dismissProgressBar();
            }
        } else {
            Toast.makeText(AddTruckActivity.this, getResources().getString(R.string.network_error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
