package com.bababazri.advosoft.bababajriMTDNew.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.activities.DriverListBookingActivity;
import com.bababazri.advosoft.bababajriMTDNew.activities.MapsActivity;
import com.bababazri.advosoft.bababajriMTDNew.fragments.BaseFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.fragments.RequestFragment;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.RequestListWrapper;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Advosoft2 on 3/8/2017.
 */

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.MyViewHolder> implements Filterable, WebCompleteTask {
    private List<RequestListWrapper> feedsListall;
    private List<RequestListWrapper> mArrayList;
    private Context context;
    private LayoutInflater inflater;
    int selectedPosition = -1;
    public int deleteCartPos = -1;
    int position_select = 0;
    BaseFragment baseFragment = null;
    ArrayList<String> imglist;
    String urlAllJob = "";
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int CONTENT_VIEW2 = 2;
    public static final String KEY_propertyID = "id";
    String id, req_id;
    RequestQueue queue;
    ProgressDialog pd;
    JSONObject object_data;

    public RequestListAdapter(Context context, List<RequestListWrapper> feedsListall) {
        this.context = context;
        this.feedsListall = feedsListall;
        this.mArrayList = feedsListall;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pd = new ProgressDialog(context);
        queue = Volley.newRequestQueue(context);
        try{
            object_data=new JSONObject(MyApplication.getValueData(Constants.ValueData)).getJSONObject("success").getJSONObject("data");
        }catch (Exception e){

        }

    }

    public void setList(List<RequestListWrapper> BindAllJobList) {
        if (BindAllJobList != null)
            this.feedsListall.addAll(BindAllJobList);
        else
            this.feedsListall = BindAllJobList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = 0;
        layoutRes = R.layout.request_list_item;
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    feedsListall = mArrayList;
                } else {

                    ArrayList<RequestListWrapper> filteredList = new ArrayList<>();

                    for (RequestListWrapper androidVersion : mArrayList) {
                        if (androidVersion.getPrice().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        } else if (androidVersion.getPrice().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    feedsListall = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = feedsListall;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                feedsListall = (ArrayList<RequestListWrapper>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void clearList() {
        if (feedsListall != null)
            feedsListall.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final RequestListWrapper feeds = feedsListall.get(position);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat target_date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        target_date.setTimeZone(tz);
        try {
            Date startDate = df.parse(feeds.getOrderDate());
            String newDateString = df.format(startDate);
            String readReminderdate = target_date.format(startDate);

            holder.date.setText(readReminderdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.product.setText(feeds.getProduct() + " " + feeds.getProduct_cat() + " " +object_data.optString(feeds.getSubType().toLowerCase())  + "\n" + feeds.getAddress().trim());
        holder.quantity.setText(feeds.getOrderQuantity().trim() + " " +object_data.optString(feeds.getUnit().trim().toLowerCase()));
        holder.distance.setText(context.getResources().getString(R.string.distance) + feeds.getDistance()+" "+context.getResources().getString(R.string.km));
        holder.fare.setText(context.getResources().getString(R.string.fare_rs) + " "+feeds.getPrice().trim());
        if (feeds.getBookingStatus().compareTo("Not Confirmed")==0){
            holder.order_status.setText(context.getResources().getString(R.string.order) + context.getResources().getString(R.string.not_confirm));
        }else {
            holder.order_status.setText(context.getResources().getString(R.string.order) + context.getResources().getString(R.string.confirm));
        }

        holder.drop_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, MapsActivity.class).putExtra("latlng", feeds.getLatlng()).putExtra("address", feeds.getAddress()));
            }
        });
        holder.cus_name.setText(context.getResources().getString(R.string.cust_name) + feeds.getCus_name());
        holder.cus_number.setText(context.getResources().getString(R.string.cust_mob) + feeds.getCus_number());
        holder.load_type.setText(feeds.getLoadType());
        holder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_id = feeds.getId();
                callApiApply(feeds.getId());

            }
        });
        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiCancel(feeds.getId());
            }
        });


/*
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, JobdetailActivity.class).putExtra("id", feeds.getId()).putExtra("data", feeds));


            }
        });*/


    }

    public void removeBindAllJob() {
        if (deleteCartPos >= 0) {
            feedsListall.remove(deleteCartPos);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return feedsListall.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView load_type, product, date, quantity, distance, fare, drop_location, order_status, driver_status, cus_name, cus_number;
        private Button btn_accept, btn_cancel;


        public MyViewHolder(View itemView) {
            super(itemView);
            load_type = (TextView) itemView.findViewById(R.id.load_type);
            product = (TextView) itemView.findViewById(R.id.product);
            date = (TextView) itemView.findViewById(R.id.date);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            distance = (TextView) itemView.findViewById(R.id.distance);
            fare = (TextView) itemView.findViewById(R.id.fare);
            btn_accept = (Button) itemView.findViewById(R.id.btn_accept);
            btn_cancel = (Button) itemView.findViewById(R.id.btn_cancel);
            drop_location = (TextView) itemView.findViewById(R.id.drop_location);
            order_status = (TextView) itemView.findViewById(R.id.order_status);
            driver_status = (TextView) itemView.findViewById(R.id.driver_status);
            cus_name = (TextView) itemView.findViewById(R.id.cus_name);
            cus_number = (TextView) itemView.findViewById(R.id.cus_number);


        }
    }

    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + "day ago ";
            } else {
                time = diffDays + "days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + "hr ago";
                } else {
                    time = diffHours + "hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + "min ago";
                    } else {
                        time = diffMinutes + "mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + "secs ago";
                    }
                }

            }

        }
        return time;
    }

    private void callApiApply(String driverid) {
        HashMap object = new HashMap();
        object.put("requestId", driverid);
        object.put("status", "Confirm");

        new WebTask(context, WebUrls.BASE_URL + WebUrls.Api_BookingConfrm, object, RequestListAdapter.this, RequestCode.CODE_VehicalAssign, 1);
    }

    private void callApiCancel(String driverid) {
        HashMap object = new HashMap();
        object.put("requestId", driverid);
        object.put("status", "Cancel");

        new WebTask(context, WebUrls.BASE_URL + WebUrls.Api_BookingConfrm, object, RequestListAdapter.this, RequestCode.CODE_BookingCancel, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_VehicalAssign) {
            context.startActivity(new Intent(context, DriverListBookingActivity.class).putExtra("id", req_id));
        } else if (taskcode == RequestCode.CODE_BookingCancel) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new RequestFragment())
                    .commit();
        }

    }

}
