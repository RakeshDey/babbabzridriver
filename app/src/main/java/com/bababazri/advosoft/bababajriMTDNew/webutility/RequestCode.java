package com.bababazri.advosoft.bababajriMTDNew.webutility;

/**
 * Created by suarebits on 3/12/15.
 */
public class RequestCode {
    public static final int CODE_Register = 1;
    public static final int CODE_Login = 2;
    public static final int CODE_Resend = 3;
    public static final int CODE_VerifyOtp = 4;
    public static final int CODE_Forgot = 5;
    public static final int CODE_GetMaterial = 6;
    public static final int CODE_AddProduct = 7;
    public static final int CODE_VehicalType = 8;
    public static final int CODE_MyVehical = 9;
    public static final int CODE_VehicalAssign = 10;
    public static final int CODE_DriverStatus = 11;
    public static final int CODE_AgentRequest = 12;
    public static final int CODE_ChangePassword = 13;
    public static final int CODE_Logout = 14;
    public static final int CODE_BookingCancel = 15;
    public static final int CODE_GetBadge = 16;
    public static final int CODE_ReadNoti= 17;
    public static final int CODE_GetAvailable = 18;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 101;
    public static final int CODE_ChangeLng = 19;
    public static final int CODE_GetSubobj = 20;
    public static final int CODE_GetBlock = 21;

}
