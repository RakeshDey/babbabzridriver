package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.RequestListWrapper;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/3/2018.
 */

public class AgentDetail extends BaseActiivty {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.material_name)
    TextView materialName;
    @Bind(R.id.quantity)
    TextView quantity;
    @Bind(R.id.price)
    TextView price;
    @Bind(R.id.drop_location)
    TextView dropLocation;
    @Bind(R.id.pickup_location)
    TextView pickupLocation;
    @Bind(R.id.req_date)
    TextView reqDate;
    @Bind(R.id.del_date)
    TextView delDate;
    @Bind(R.id.req_from)
    TextView reqFrom;
    @Bind(R.id.cus_name)
    TextView cusName;
    @Bind(R.id.cus_mob)
    TextView cusMob;
    @Bind(R.id.cus_address)
    TextView cusAddress;
    @Bind(R.id.btn_trpstart)
    Button btnTrpstart;
    RequestListWrapper feeds;
    @Bind(R.id.driver_name)
    TextView driverName;
    @Bind(R.id.driver_mob_nmbr)
    TextView driverMobNmbr;
    String status;
    JSONObject object_data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.request_detail));
        try {
            object_data = new JSONObject(MyApplication.getValueData(Constants.ValueData)).getJSONObject("success").getJSONObject("data");
        } catch (Exception e) {

        }
        feeds = (RequestListWrapper) getIntent().getSerializableExtra("data");
        status = getIntent().getStringExtra("status");
        if (status.compareTo("verified") == 0) {
            btnTrpstart.setVisibility(View.GONE);
        } else {
            btnTrpstart.setVisibility(View.VISIBLE);
        }

        materialName.setText(feeds.getProduct() + " " + feeds.getProduct_cat());
        quantity.setText(feeds.getOrderQuantity() + " " + object_data.optString(feeds.getUnit().trim().toLowerCase()));
        price.setText(feeds.getPrice());
        dropLocation.setText(feeds.getAddress());
        pickupLocation.setText(feeds.getSrc_address());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat target_date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        target_date.setTimeZone(tz);
        try {
            Date startDate = df.parse(feeds.getOrderDate());
            Date startDate1 = df.parse(feeds.getDeliveryDate());
            String newDateString = df.format(startDate);
            String readReminderdate = target_date.format(startDate);
            reqDate.setText(readReminderdate);

            String readReminderdate1 = target_date.format(startDate1);
            delDate.setText(readReminderdate1);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (feeds.getProductId().compareTo("") != 0) {
            reqFrom.setText(R.string.owner);
        } else {
            reqFrom.setText(R.string.customer);
        }
        cusName.setText(feeds.getCus_name());
        cusMob.setText(feeds.getCus_number());
        cusAddress.setText(feeds.getAddress());
        driverName.setText(feeds.getDriver_name());
        driverMobNmbr.setText(feeds.getDriver_number());
        btnTrpstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AgentDetail.this, ApproveByAgentActivity.class).putExtra("data", feeds));
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
