package com.bababazri.advosoft.bababajriMTDNew.custom;

/**
 * Created by Advosoft2 on 1/23/2018.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;


public class HintAdapter extends ArrayAdapter<String> {

    public HintAdapter(Context context, int resource, String[] objects) {
        super(context, R.layout.item_spinner, objects);
    }

    @Override
    public boolean isEnabled(int position) {
        if (position == 0) {
            return false;
        } else {
            return true;
        }
    }

    public void setHeader(String[] data) {

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        TextView tv = (TextView) view;
        if (position == 0) {
            tv.setTextColor(Color.GRAY);
        }
        Float width = getContext().getResources().getDimension(R.dimen._10sdp);
        Drawable drawable = getContext().getResources().getDrawable(R.drawable.ic_drop_down);
        drawable.setBounds(0, 0, width.intValue(), width.intValue());
        tv.setCompoundDrawables(null, null, drawable, null);
        tv.invalidate();
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView tv = (TextView) view;
        if (position == 0) {
            tv.setTextColor(Color.GRAY);
            Float width = getContext().getResources().getDimension(R.dimen._10sdp);
            Drawable drawable = getContext().getResources().getDrawable(R.drawable.ic_drop_down);
            drawable.setBounds(0, 0, width.intValue(), width.intValue());
            tv.setCompoundDrawables(null, null, drawable, null);
        } else {
            tv.setTextColor(Color.BLACK);
            tv.setCompoundDrawables(null, null, null, null);
        }
        return view;
    }


}