package com.bababazri.advosoft.bababajriMTDNew.wrapper;


public class FirebaseLocation {

    public Double currntLong;

    public String jobId;

    public String jobStatus;

    public String driverId;

    public Double currntLatt;

    public long timestamp;



    public FirebaseLocation(String jobId, Double currntLatt, Double currntLong, String jobStatus, String driverId, long timestamp) {
        this.currntLong = currntLong;
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.driverId = driverId;
        this.currntLatt = currntLatt;
        this.timestamp = timestamp;
    }

    public FirebaseLocation() {
    }


}