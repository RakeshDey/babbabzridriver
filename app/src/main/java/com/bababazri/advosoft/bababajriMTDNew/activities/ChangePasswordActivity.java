package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/4/2018.
 */

public class ChangePasswordActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.old_password)
    EditText oldPassword;
    @Bind(R.id.new_password)
    EditText newPassword;
    @Bind(R.id.cnm_password)
    EditText cnmPassword;
    @Bind(R.id.btn_submit)
    Button btnSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.change_password);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oldPassword.getText().toString().isEmpty()) {
                    oldPassword.setError(getResources().getString(R.string.notempty));
                } else if (newPassword.getText().toString().isEmpty()) {
                    newPassword.setError(getResources().getString(R.string.notempty));
                } else if (cnmPassword.getText().toString().isEmpty()) {
                    cnmPassword.setError(getResources().getString(R.string.notempty));
                } else if (newPassword.getText().toString().length() < 6) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.password_shdnot_less, Toast.LENGTH_SHORT).show();
                } else if (newPassword.getText().toString().compareTo(cnmPassword.getText().toString()) != 0) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.password_not_matched, Toast.LENGTH_SHORT).show();
                } else {
                    callapiAll();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        objectNew.put("oldPassword", oldPassword.getText().toString());
        objectNew.put("newPassword", newPassword.getText().toString());
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(ChangePasswordActivity.this, WebUrls.BASE_URL + WebUrls.Api_ChangePassword, objectNew, ChangePasswordActivity.this, RequestCode.CODE_ChangePassword, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_ChangePassword) {
            Toast.makeText(ChangePasswordActivity.this, R.string.password_change_successfully, Toast.LENGTH_SHORT).show();

        }
    }
}
