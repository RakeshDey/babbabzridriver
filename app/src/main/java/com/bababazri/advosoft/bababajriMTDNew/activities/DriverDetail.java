package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.DriverRequestWrapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/3/2018.
 */

public class DriverDetail extends BaseActiivty {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.material_name)
    TextView materialName;
    @Bind(R.id.quantity)
    TextView quantity;
    @Bind(R.id.price)
    TextView price;
    @Bind(R.id.drop_location)
    TextView dropLocation;
    @Bind(R.id.pickup_location)
    TextView pickupLocation;
    @Bind(R.id.req_date)
    TextView reqDate;
    @Bind(R.id.del_date)
    TextView delDate;
    @Bind(R.id.req_from)
    TextView reqFrom;
    @Bind(R.id.cus_name)
    TextView cusName;
    @Bind(R.id.cus_mob)
    TextView cusMob;
    @Bind(R.id.cus_address)
    TextView cusAddress;
    @Bind(R.id.btn_trpstart)
    Button btnTrpstart;
    DriverRequestWrapper feeds;
    @Bind(R.id.call)
    ImageView call;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.request_detail);
        feeds = (DriverRequestWrapper) getIntent().getSerializableExtra("data");
        materialName.setText(feeds.getMaterialname() + " " + feeds.getMaterialTypeName());
        quantity.setText(feeds.getOrderQuantity() + " " + feeds.getUnit());
        price.setText(feeds.getPrice());
        dropLocation.setText(feeds.getCustomerAddress());
        pickupLocation.setText(feeds.getPickupaddress());

        if (feeds.getBookingStatus().compareTo("Confirm") == 0) {
            btnTrpstart.setVisibility(View.VISIBLE);
        } else {
            btnTrpstart.setVisibility(View.GONE);
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat target_date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        target_date.setTimeZone(tz);
        try {
            Date startDate = df.parse(feeds.getOrderDate());
            Date startDate1 = df.parse(feeds.getDeliveryDate());
            String newDateString = df.format(startDate);
            String readReminderdate = target_date.format(startDate);
            reqDate.setText(readReminderdate);

            String readReminderdate1 = target_date.format(startDate1);
            delDate.setText(readReminderdate1);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (feeds.getProductId() != null&&feeds.getProductId().compareTo("")!=0) {
            reqFrom.setText(R.string.owner);
        } else {
            reqFrom.setText(R.string.customer);
        }
        cusName.setText(feeds.getCusName());
        cusMob.setText(feeds.getCusMob());
        cusAddress.setText(feeds.getCustomerAddress());
        btnTrpstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*try{
                    JSONObject des_obj = new JSONObject(feeds.getCustomerLoaction());
                    double lat=des_obj.optDouble("lat");
                    double lng=des_obj.optDouble("lng");
                   // String format = "geo:0,0?q=" + lat + "," + lng + "( Location title)";
                    String format = "google.navigation:q=" +lat+","+lng;

                    Uri uri = Uri.parse(format);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setPackage("com.google.android.apps.maps");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }catch (Exception e){

                }*/

                startActivity(new Intent(DriverDetail.this, DriverHomeActivity.class).putExtra("data", feeds));
                finish();


            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + feeds.getCusMob()));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void stopActivity(){
        finish();
    }
}
