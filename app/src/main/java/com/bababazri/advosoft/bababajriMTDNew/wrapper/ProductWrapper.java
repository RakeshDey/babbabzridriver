package com.bababazri.advosoft.bababajriMTDNew.wrapper;

import java.io.Serializable;

/**
 * Created by advosoft on 7/19/2018.
 */

public class ProductWrapper implements Serializable {
    String id;
    String materialName;
    String materialSubcat;
    String address;
    String price;
    String unit;
    String subType;
    String venderName;
    String productImg;
    String productStatus;

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getProductImg() {
        return productImg;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getVenderName() {
        return venderName;
    }

    public void setVenderName(String venderName) {
        this.venderName = venderName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialSubcat() {
        return materialSubcat;
    }

    public void setMaterialSubcat(String materialSubcat) {
        this.materialSubcat = materialSubcat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
