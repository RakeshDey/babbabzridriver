package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.ProductWrapper;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditProduct extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.txt_cat)
    TextView txtCat;
    @Bind(R.id.cat_ll)
    LinearLayout catLl;
    @Bind(R.id.txt_sub_cat)
    TextView txtSubCat;
    @Bind(R.id.sb_cat_ll)
    LinearLayout sbCatLl;
    @Bind(R.id.prod_img)
    ImageView prodImg;
    @Bind(R.id.txt_sub_type)
    TextView txtSubType;
    @Bind(R.id.sub_type_ll)
    LinearLayout subTypeLl;
    @Bind(R.id.vendor_name)
    EditText vendorName;
    @Bind(R.id.address)
    TextView address;
    @Bind(R.id.price)
    EditText price;
    @Bind(R.id.txt_unit)
    TextView txtUnit;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    private ProductWrapper feedsListall;
    JSONObject object_data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.edit_product);
        feedsListall = (ProductWrapper) getIntent().getSerializableExtra("data");
        try {
            object_data = new JSONObject(MyApplication.getValueData(Constants.ValueData)).getJSONObject("success").getJSONObject("data");
        } catch (Exception e) {

        }

        txtCat.setText(feedsListall.getMaterialName());
        txtSubCat.setText(feedsListall.getMaterialSubcat());
        txtUnit.setText(object_data.optString(feedsListall.getUnit().trim().toLowerCase()));
        price.setText(feedsListall.getPrice());
        if (feedsListall.getSubType().compareTo("") != 0) {
            txtSubType.setText(object_data.optString(feedsListall.getSubType().trim().toLowerCase()));
        } else {
            subTypeLl.setVisibility(View.GONE);
        }
        vendorName.setText(feedsListall.getVenderName());
        address.setText(feedsListall.getAddress());
        if (feedsListall.getProductImg().compareTo("") != 0) {
            Utils.loadImage(EditProduct.this, prodImg, WebUrls.BASE_URL + feedsListall.getProductImg());
        }

        vendorName.setEnabled(false);
        address.setEnabled(false);

        prodImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedsListall.getProductImg().compareTo("") != 0) {
                    MyApplication.showFullImage(EditProduct.this, WebUrls.BASE_URL + feedsListall.getProductImg());
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calladdProduct();
            }
        });
    }

    private void calladdProduct() {
        HashMap objectNew = new HashMap();
        objectNew.put("productId", feedsListall.getId());
        objectNew.put("price", price.getText().toString());
        new WebTask(EditProduct.this, WebUrls.BASE_URL + WebUrls.Api_EditProduct, objectNew, EditProduct.this, RequestCode.CODE_AddProduct, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode==RequestCode.CODE_AddProduct){
            Toast.makeText(EditProduct.this,R.string.product_update,Toast.LENGTH_SHORT).show();
            finish();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
