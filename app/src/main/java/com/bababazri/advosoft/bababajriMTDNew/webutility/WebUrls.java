package com.bababazri.advosoft.bababajriMTDNew.webutility;

/**
 * Created by Advosoft2 on 6/28/2017.
 */
public class WebUrls {
    public static final String BASE_URL = "https://api.babbabazri.com";
    // public static final String BASE_URL = "http://139.59.71.150:3010";
    // public static final String BASE_URL = "http://192.168.1.52:8080/escort/api";
    public static final String BASE_URL2 = "https://api.babbabazri.com";
    // public static final String BASE_URL2 = "http://139.59.71.150:3010";
    // public static final String BASE_URL2 = "http://192.168.1.52:8080/escort";
    public static final String Api_Signup = "/api/People/signup";
    public static final String Api_Resendotp = "/api/Otps/resendOtp";
    public static final String Api_VerifyOtp = "/api/Otps/verifyMobile";
    public static final String Api_Login = "/api/People/login";
    public static final String Api_GetMaterial = "/api/Materials/getMaterials";
    public static final String Api_AddProduct = "/api/Products/addProduct";
    public static final String Api_GetVehicalType = "/api/VehicleTypes/getTypes";
    public static final String Api_GetMyVehical = "/api/Vehicles/getMyVehicles";
    public static final String Api_GetMyDriver = "/api/People/getDriver";
    public static final String Api_GetRequestList = "/api/OrderRequests/requestList";
    public static final String Api_VehicalAssign = "/api/People/assignVehicle";
    public static final String Api_DriverListBooking = "/api/People/driverForBooking";
    public static final String Api_BookingCnfrm = "/api/OrderRequests/acceptRejectRequest";
    public static final String Api_GetOredrList = "/api/OrderRequests/orderList";
    public static final String Api_DriverAvail = "/api/People/driverAvailability";
    public static final String Api_GetUserProfile = "/api/People/userById";
    public static final String Api_DriverRequestList = "/api/OrderRequests/driverOrderList";
    public static final String Api_DriverStatus = "/api/OrderRequests/tripStatus";
    public static final String Api_AgentRequest = "/api/OrderRequests/agentNotificationList";
    public static final String Api_ChangePassword = "/api/People/change-password";
    public static final String Api_BookingConfrm = "/api/OrderRequests/confirmOrder";
    public static final String Api_DriverHistory = "/api/OrderRequests/driverTripHistory";
    public static final String Api_VerificationByAgent = "/api/OrderRequests/orderVerifyByAgent";
    public static final String Api_Logout = "/api/People/logout";
    public static final String Api_GetNoti = "/api/Notifications/getNotificationList";
    public static final String Api_GetBadge = "/api/Notifications/getBadgeCount";
    public static final String Api_ReadNoti = "/api/Notifications/readAllNotifications";
    public static final String Api_GetAvailbale = "/api/People/getDriverAvailabilityData";
    public static final String Api_ProductList = "/api/Products/getMyProduct";
    public static final String Api_Product_Delete = "/api/Products/deleteProd";
    public static final String Api_DriverBlock = "/api/People/blockUser";
    public static final String Api_ChangeLng = "/api/People/changeLanguage";
    public static final String Api_SubtypeObj = "/api/SubTypes/getSubTypesObj";
    public static final String Api_Forget = "/api/People/resetPassRequest";
    public static final String Api_ResetVerify = "/api/Otps/checkResetOtp";
    public static final String Api_ResetPassword = "/api/People/resetPassword";
    public static final String Api_EditProduct = "/api/Products/updateProduct";
    // server url  link :-
/* public static final String BASE_URL = "http://laravel.meetnepali.com/api";
 public static final String BASE_URL2 = "http://laravel.meetnepali.com";*/
    public static final String BASE_URL_image = "http://api.babbabazri.com/public/uploads/";
    //  public static final String BASE_URL_image = "http://demoadvoco.in/escortapp/public/uploads/";
    //  public static final String BASE_URL_image = "http://192.168.1.52:8080/escort/public/uploads/";


    public final static String Add_Post = "/api/Posts/addPost";

    public static final String UpdateProfile = "/api/People/updateProfile";
    public static final String AddDriver = "/api/People/driverReg";
    public static final String AddTruck = "/api/Vehicles/addVehicle";
    public static final String AddProduct = "/api/Products/addProduct";
    public static final String DriverVisi = "/api/People/driverAvailability";
    public final static String Add_SendImage = "/api/Chats/sendImgVdo";

}
