package com.bababazri.advosoft.bababajriMTDNew.Service;

/**
 * Created by Advosoft2 on 2/21/2018.
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.ArrayMap;
import android.util.Log;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.activities.DriverHome;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;

public class LocationTracker extends Service {
    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;
    public static final int LOCATION_INTERVAL = 1000;
    public static final float LOCATION_DISTANCE = 0f;
    public static Location locationupdate;
    private Context context;
    String tripId;

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }


        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            locationupdate = location;

            MyApplication.setDouble(Constants.SP_NEW_LAT, (mLastLocation.getLatitude()));
            MyApplication.setDouble(Constants.SP_NEW_LONG, (mLastLocation.getLongitude()));
            Log.e("location", location.toString());
/*
            JSONObject param = new JSONObject();
            JSONObject locationObject = new JSONObject();

            try {
                locationObject.put("lat", location.getLatitude());
                locationObject.put("lng", location.getLongitude());

                param.put("location", locationObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            //MyApplication.setSharedPrefString(Constants.SP_DriverStatus, "start");
            Log.d("trip_status", MyApplication.getSharedPrefStringNew(Constants.SP_TripId));
            if (MyApplication.getSharedPrefStringNew(Constants.SP_TripId) != null) {
                if (MyApplication.getSharedPrefStringNew(Constants.SP_TripId).compareTo("") != 0) {
                    if (MyApplication.getSharedPrefString(Constants.SP_DriverStatus).compareTo("start") == 0) {
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations").child(MyApplication.getSharedPrefString(Constants.SP_TripId));
                        Long tsLong = System.currentTimeMillis() / 1000;
                        Map<String, Object> jsonParams = new ArrayMap<>();
                        jsonParams.put("currntLatt", mLastLocation.getLatitude());
                        jsonParams.put("currntLong", mLastLocation.getLongitude());
                        jsonParams.put("jobStatus", "start");
                        jsonParams.put("timestamp", tsLong);
                        mDatabase.updateChildren(jsonParams);

                        //  mDatabase.child(MyApplication.getSharedPrefString(Constants.SP_CURRENT_JOB)).child("currntLatt").setValue(location.getLatitude());
                        // mDatabase.child(MyApplication.getSharedPrefString(Constants.SP_CURRENT_JOB)).child("currntLong").setValue(location.getLongitude());
                    } else if (MyApplication.getSharedPrefString(Constants.SP_DriverStatus).compareTo("end") == 0) {
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations").child(MyApplication.getSharedPrefString(Constants.SP_TripId));
                        Long tsLong = System.currentTimeMillis() / 1000;
                        Map<String, Object> jsonParams = new ArrayMap<>();
                        jsonParams.put("currntLatt", mLastLocation.getLatitude());
                        jsonParams.put("currntLong", mLastLocation.getLongitude());
                        jsonParams.put("jobStatus", "end");
                        jsonParams.put("timestamp", tsLong);
                        mDatabase.updateChildren(jsonParams);
                    }
                }

            }


            Intent in = new Intent("locationUpdated");
            in.putExtra("location", location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);


//        updateNotification("Lat: " + g╥etLatitude() + " Long: " + getLongitude());

           /* ApiService.getInstance().makePostCallAuth(context, ApiService.UPDATE_DRIVER_LOCATION, param, new ApiService.OnResponse() {
                @Override
                public void onResponseSuccess(JSONObject response) {
//                    MyApplication.showToast(context, "Location Updated");
                }

                @Override
                public void onError(VolleyError volleyError) {
                    MyApplication.showToast(context, "Update Location Failed");
                }
            });*/

        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean finish = intent.getBooleanExtra("finish", false);
        tripId = intent.getStringExtra("id");
        Log.d("tripId", intent.getStringExtra("id"));
        if (finish) {
            stopForeground(true);
            stopSelf();
        }
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        context = getApplicationContext();
        Intent playIntent = new Intent(getApplicationContext(), DriverHome.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                playIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name) + " is running")
                .setContentText(getResources().getString(R.string.app_name) + " is running")
                .setSmallIcon(R.mipmap.ic_launcher)
                //.setContentIntent(pendingIntent)
                .setOngoing(true)
                .setAutoCancel(true)
                .build();


        startForeground(1001,
                notification);

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }

    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }
}