package com.bababazri.advosoft.bababajriMTDNew.webutility;


import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiConfig {

    @Multipart
    @POST("retrofit_example/upload_image.php")
    Call<JsonObject> uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);

    //String  token=PostPropertyFragment.accesstokrn;
    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> UpdateProfile(@Part MultipartBody.Part dl,
                                   @Part("data") RequestBody lookingFor,
                                   @Header("Authorization") String accessToken,
                                   @Header("ln") String ln
    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> UpdateProfileNew(
            @Part("data") RequestBody lookingFor,
            @Header("Authorization") String accessToken,
            @Header("ln") String ln
    );

    @Multipart
    @POST(WebUrls.AddDriver)
    Call<JsonObject> AddDriver(@Part MultipartBody.Part dl,
                               @Part MultipartBody.Part aadhar_card,
                               @Part MultipartBody.Part photo,
                               @Part MultipartBody.Part policeVrification,
                               @Part("data") RequestBody lookingFor,
                               @Header("Authorization") String accessToken,
                               @Header("ln") String ln
    );

    @Multipart
    @POST(WebUrls.AddTruck)
    Call<JsonObject> AddTruck(@Part MultipartBody.Part polution,
                              @Part MultipartBody.Part rc,
                              @Part MultipartBody.Part insurance,
                              @Part MultipartBody.Part other,
                              @Part("data") RequestBody lookingFor,
                              @Header("Authorization") String accessToken,
                              @Header("ln") String ln
    );

    @Multipart
    @POST(WebUrls.AddProduct)
    Call<JsonObject> AddProduct(@Part MultipartBody.Part polution,
                                @Part("data") RequestBody lookingFor,
                                @Header("Authorization") String accessToken,
                                @Header("ln") String ln
    );

    @Multipart
    @POST(WebUrls.DriverVisi)
    Call<JsonObject> DriverVisibility(@Part MultipartBody.Part vehical,
                                      @Part MultipartBody.Part bill,
                                      @Part("data") RequestBody lookingFor,
                                      @Header("Authorization") String accessToken,
                                      @Header("ln") String ln
    );

    @Multipart
    @POST(WebUrls.Add_SendImage)
    Call<JsonObject> PostChatImage(@Part MultipartBody.Part profileImage,
                                   @Part("data") RequestBody lookingFor,
                                   @Query("access_token") String accessToken
    );

    @Multipart
    @POST(WebUrls.Add_Post)
    Call<JsonObject> PostContract(@Part MultipartBody.Part postImage,
                                  @Part("data") RequestBody lookingFor,
                                  @Query("access_token") String accessToken
    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> Updatepost(@Part MultipartBody.Part postImage,
                                @Part("request") RequestBody lookingFor

    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> addImage(@Part MultipartBody.Part files,
                              @Part("request") RequestBody lookingFor

    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> addmarketplace(@PartMap Map<String, RequestBody> files,
                                    @Part("request") RequestBody lookingFor
    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> uploadProfile(@Part MultipartBody.Part files,
                                   @Part("request") RequestBody lookingFor
    );

    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> addProductNew(@PartMap Map<String, RequestBody> files,
                                   @Part("request") RequestBody lookingFor
    );


    @Multipart
    @POST(WebUrls.UpdateProfile)
    Call<JsonObject> Updatepostdata(@Part("request") RequestBody lookingFor

    );


}
