package com.bababazri.advosoft.bababajriMTDNew.Service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.ArrayMap;
import android.util.Log;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.activities.DriverHome;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;

/**
 * Created by advosoft on 7/23/2018.
 */

public class GoogleService extends Service implements LocationListener {

    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 1000;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;
    String tripId;
    private Context context;


    public GoogleService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try{
            mTimer = new Timer();
            mTimer.schedule(new TimerTaskToGetLocation(), 5, notify_interval);
            intent = new Intent(str_receiver);
        }catch (Exception e){

        }

//        fn_getlocation();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{
           // boolean finish = intent.getBooleanExtra("finish", false);
            tripId = intent.getStringExtra("id");
            Log.d("tripId", intent.getStringExtra("id"));
          /*  if (finish) {
                stopForeground(true);
                stopSelf();
            }*/
            Log.e(TAG, "onStartCommand");
            Log.d("trip_status", MyApplication.getSharedPrefStringNew(Constants.SP_TripId));
            super.onStartCommand(intent, flags, startId);
            context = getApplicationContext();
        }catch (Exception e){

        }

        Intent playIntent = new Intent(getApplicationContext(), DriverHome.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                playIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name) + " is running")
                .setContentText(getResources().getString(R.string.app_name) + " is running")
                .setSmallIcon(R.mipmap.ic_launcher)
                //.setContentIntent(pendingIntent)
                .setOngoing(true)
                .setAutoCancel(true)
                .build();


        startForeground(1001,
                notification);

        return START_STICKY;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void fn_getlocation() {
        try {
            locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
            isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnable && !isNetworkEnable) {

            } else {

            /*if (isNetworkEnable) {
                location = null;
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                //  criteria.setPowerRequirement(Criteria.POWER_HIGH);
                criteria.setAltitudeRequired(false);
                criteria.setSpeedRequired(false);
                criteria.setCostAllowed(false);
                criteria.setBearingRequired(true);


//API level 9 and up
                criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
                criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
                locationManager.requestLocationUpdates(1000, 15, criteria, this, null);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {

                        Log.e("latitude", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");

                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        fn_update(location);
                    }
                }

            }*/


                if (isGPSEnable) {
                    location = null;
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    //  criteria.setPowerRequirement(Criteria.POWER_HIGH);
                    criteria.setAltitudeRequired(false);
                    criteria.setSpeedRequired(false);
                    criteria.setCostAllowed(false);
                    criteria.setBearingRequired(true);


//API level 9 and up
                    criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
                    criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
                    locationManager.requestLocationUpdates(1000, 15, criteria, this, null);

                    //  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                       // location=getLastKnownLocation();
                        if (location != null) {

                            Location location1 = new Location("");
                            location1.setLatitude(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)));
                            location1.setLongitude(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG)));

                            Location location2 = new Location("");
                            location2.setLatitude(location.getLatitude());
                            location2.setLongitude(location.getLongitude());
                            Log.d("distance_ervice",location1.distanceTo(location2) - location2.getAccuracy()+"");

                            if (location1.distanceTo(location2) - location2.getAccuracy() > 5) {
                                Log.e("latitude", location.getLatitude() + "");
                                Log.e("longitude", location.getLongitude() + "");
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                MyApplication.setSharedPrefString(Constants.SP_CURRENT_LAT, latitude+"");
                                MyApplication.setSharedPrefString(Constants.SP_CURRENT_LONG, longitude+"");
                                fn_update(location);
                            }


                        }
                    }
                }


            }
        } catch (Exception e) {

        }


    }

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    fn_getlocation();
                }
            });

        }
    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void fn_update(Location location) {

        try {
            MyApplication.setDouble(Constants.SP_NEW_LAT, (location.getLatitude()));
            MyApplication.setDouble(Constants.SP_NEW_LONG, (location.getLongitude()));
            Log.e("location", location.toString());

            Log.d("trip_status", MyApplication.getSharedPrefStringNew(Constants.SP_TripId));
            if (MyApplication.getSharedPrefStringNew(Constants.SP_TripId) != null) {
                if (MyApplication.getSharedPrefStringNew(Constants.SP_TripId).compareTo("") != 0) {
                    if (MyApplication.getSharedPrefString(Constants.SP_DriverStatus).compareTo("start") == 0) {
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations").child(MyApplication.getSharedPrefString(Constants.SP_TripId));
                        Long tsLong = System.currentTimeMillis() / 1000;
                        Map<String, Object> jsonParams = new ArrayMap<>();
                        jsonParams.put("currntLatt", location.getLatitude());
                        jsonParams.put("currntLong", location.getLongitude());
                        jsonParams.put("jobStatus", "start");
                        jsonParams.put("timestamp", tsLong);
                        mDatabase.updateChildren(jsonParams);

                        //  mDatabase.child(MyApplication.getSharedPrefString(Constants.SP_CURRENT_JOB)).child("currntLatt").setValue(location.getLatitude());
                        // mDatabase.child(MyApplication.getSharedPrefString(Constants.SP_CURRENT_JOB)).child("currntLong").setValue(location.getLongitude());
                    } else if (MyApplication.getSharedPrefString(Constants.SP_DriverStatus).compareTo("end") == 0) {
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations").child(MyApplication.getSharedPrefString(Constants.SP_TripId));
                        Long tsLong = System.currentTimeMillis() / 1000;
                        Map<String, Object> jsonParams = new ArrayMap<>();
                        jsonParams.put("currntLatt", location.getLatitude());
                        jsonParams.put("currntLong", location.getLongitude());
                        jsonParams.put("jobStatus", "end");
                        jsonParams.put("timestamp", tsLong);
                        mDatabase.updateChildren(jsonParams);
                        MyApplication.setSharedPrefString(Constants.SP_DriverStatus, "");
                        MyApplication.setSharedPrefStringNew(Constants.SP_TripId, "");
                    }
                }

            }


            intent.putExtra("location", location);
            intent.putExtra("latutide", location.getLatitude() + "");
            intent.putExtra("longitude", location.getLongitude() + "");
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            //sendBroadcast(intent);
        } catch (Exception e) {

        }

    }


}
