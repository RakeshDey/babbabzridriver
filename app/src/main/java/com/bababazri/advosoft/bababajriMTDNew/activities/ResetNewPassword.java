package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 10/30/2018.
 */

public class ResetNewPassword extends BaseActiivty implements WebCompleteTask{
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.new_password)
    EditText newPassword;
    @Bind(R.id.new_confirm_password)
    EditText newConfirmPassword;
    @Bind(R.id.new_submit_btn)
    Button newSubmitBtn;
    String id;
    String otp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        id = getIntent().getStringExtra("id");
        otp = getIntent().getStringExtra("otp");

        newSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(newPassword.getText().toString())) {
                    newPassword.setError(getString(R.string.notempty));
                    newPassword.requestFocus();
                } else if (newPassword.getText().length() < 6) {
                    newPassword.setError(getString(R.string.password_shdnot_less));
                    newPassword.requestFocus();
                } else if (TextUtils.isEmpty(newConfirmPassword.getText().toString())) {
                    newConfirmPassword.setError(getString(R.string.notempty));
                    newConfirmPassword.requestFocus();
                } else if (newPassword.getText().toString().compareTo(newConfirmPassword.getText().toString()) != 0) {
                    Toast.makeText(ResetNewPassword.this, R.string.password_not_matched, Toast.LENGTH_SHORT).show();
                } else {
                    callResetPassword();
                }
            }
        });
    }


    private void callResetPassword() {
        HashMap objectNew = new HashMap();
        objectNew.put("id", id);
        objectNew.put("otp", otp);
        objectNew.put("newPassword",newPassword.getText().toString());
        objectNew.put("ln", MyApplication.getlangID(Constants.LangId));

        new WebTask(ResetNewPassword.this, WebUrls.BASE_URL + WebUrls.Api_ResetPassword, objectNew, ResetNewPassword.this, RequestCode.CODE_Resend, 1);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response",response);
        if (taskcode==RequestCode.CODE_Resend){
            Toast.makeText(ResetNewPassword.this, R.string.password_reset, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ResetNewPassword.this, LoginActivity.class));
            finish();
        }

    }
}
