package com.bababazri.advosoft.bababajriMTDNew.fragments;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.ArrayMap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Map;

import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/29/2018.
 */

public class MapsFragment extends BaseFragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Boolean data_check=false;

    public MapsFragment() {
    }

    public static MapsFragment getInstance(Bundle bundle) {
        MapsFragment chef = new MapsFragment();
        chef.setArguments(bundle);
        return chef;
    }

    Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }


    @Override
    protected void initUi(View view) {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        mapFragment.getMapAsync(MapsFragment.this);
    }

    @Override
    protected void setValueOnUi() {

    }

    @Override
    protected void setListener() {

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    @Override
    public void onClick(View view) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_maps, container, false);
        ButterKnife.bind(this, view);

        getActivity().setTitle(R.string.app_name);
        if (MyApplication.getUserLoginType(Constants.LoginType).compareTo("agent") == 0) {
            disableBackButton();
        } else {
            disbleBackButtonIcon();
        }


        initUi(view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.d("width of screen:-", "" + width);
        Log.d("height of screen:-", "" + height);
        // setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Double lat = 0.0, lng = 0.0;

       /* GPSTracker tracker = new GPSTracker(getActivity());
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
        }*/
        mMap.setMyLocationEnabled(true);



        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                try{
                    LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(sydney).title("Current Location"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Driver").child(MyApplication.getUserID(Constants.UserID));
                    MyApplication.setSharedPrefString(Constants.SP_CURRENT_LAT, (location.getLatitude())+"");
                    MyApplication.setSharedPrefString(Constants.SP_CURRENT_LONG, (location.getLongitude()+""));

                    Long tsLong = System.currentTimeMillis() / 1000;
                    Map<String, Object> jsonParams = new ArrayMap<>();
                    jsonParams.put("lat", location.getLatitude());
                    jsonParams.put("lng", location.getLongitude());
                    jsonParams.put("timestamp", tsLong);
                    mDatabase.updateChildren(jsonParams);
                }catch (Exception e){

                }



               /* CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(sydney)
                        .zoom(10).build();
                //Zoom in and animate the camera.
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
            }
        });
        // Add a marker in Sydney and move the camera


    }


}
