package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.ApiConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.AppConfig;
import com.google.gson.JsonObject;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by advosoft on 6/16/2018.
 */

public class AddDriverActivity extends BaseActiivty {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.edit_name)
    EditText editName;
    @Bind(R.id.edit_last_name)
    EditText editLastName;
    @Bind(R.id.edit_mobile)
    EditText editMobile;
    @Bind(R.id.edit_password)
    EditText editPassword;
    @Bind(R.id.edit_confirm_pass)
    EditText editConfirmPass;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    @Bind(R.id.address)
    PlacesAutocompleteTextView address;
    PlaceDetails addressplace;
    JSONObject addressJson = null;
    String addressId;
    String filepath = null;
    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    @Bind(R.id.img_licence)
    ImageView imgLicence;
    @Bind(R.id.ll_log)
    LinearLayout llLog;
    @Bind(R.id.img_aadhar_card)
    ImageView imgAadharCard;
    @Bind(R.id.ll_insurance)
    LinearLayout llInsurance;
    @Bind(R.id.ll_image)
    LinearLayout llImage;
    @Bind(R.id.img_photo)
    ImageView imgPhoto;
    @Bind(R.id.img_police)
    ImageView imgPolice;
    int image_type = 0;
    String file_licence = "", file_aadharcard = "", file_photo = "", file_police = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_driver);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.add_driver);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        address.setLanguageCode(MyApplication.getlangID(Constants.LangId));
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editName.getText().toString().isEmpty()) {
                    editName.setError(getString(R.string.notempty));
                    editName.setFocusable(true);
                } else if (editLastName.getText().toString().isEmpty()) {
                    editLastName.setError(getString(R.string.notempty));
                    editLastName.setFocusable(true);
                } else if (editMobile.getText().toString().isEmpty()) {
                    editMobile.setError(getString(R.string.notempty));
                    editMobile.setFocusable(true);
                } else if (editMobile.getText().toString().length() != 10) {
                    Toast.makeText(AddDriverActivity.this, R.string.mobile_not_valid, Toast.LENGTH_SHORT).show();
                } else if (editPassword.getText().toString().isEmpty()) {
                    editPassword.setError(getString(R.string.notempty));
                    editPassword.setFocusable(true);
                } else if (editConfirmPass.getText().toString().isEmpty()) {
                    editConfirmPass.setError(getString(R.string.notempty));
                    editConfirmPass.setFocusable(true);
                } else if (editPassword.getText().toString().length() < 6) {
                    Toast.makeText(AddDriverActivity.this, R.string.password_shdnot_less, Toast.LENGTH_SHORT).show();
                } else if (editPassword.getText().toString().compareTo(editConfirmPass.getText().toString()) != 0) {
                    Toast.makeText(AddDriverActivity.this, R.string.password_not_matched, Toast.LENGTH_SHORT).show();
                } else if (addressJson == null) {
                    Toast.makeText(AddDriverActivity.this, R.string.please_select_address, Toast.LENGTH_SHORT).show();
                } else if (file_licence.compareTo("") == 0) {
                    Toast.makeText(AddDriverActivity.this, R.string.please_select_driving_lcnc, Toast.LENGTH_SHORT).show();
                } else if (file_aadharcard.compareTo("") == 0) {
                    Toast.makeText(AddDriverActivity.this, R.string.pls_select_gov_id, Toast.LENGTH_SHORT).show();
                } else if (file_photo.compareTo("") == 0) {
                    Toast.makeText(AddDriverActivity.this, R.string.pls_select_photo, Toast.LENGTH_SHORT).show();
                } /*else if (file_police.compareTo("") == 0) {
                    Toast.makeText(AddDriverActivity.this, "please select verification image", Toast.LENGTH_SHORT).show();
                }*/ else {
                    validateViews();
                }
            }
        });

        address.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull final Place place) {
                addressId = place.place_id;
//                MyApplication.showProgressDialog("Getting Location");
                address.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                        addressplace = placeDetails;

                        addressJson = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            addressJson.put("address", placeDetails.formatted_address);
                            addressJson.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                    }
                });
            }
        });

        imgLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 1;
                uploadImageTypePost();
            }
        });
        imgAadharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 2;
                uploadImageTypePost();
            }
        });
        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 3;
                uploadImageTypePost();
            }
        });
        imgPolice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 4;
                uploadImageTypePost();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void uploadImageTypePost() {
        setimagepath();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(R.string.choose_option);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.select_img_from)
                .setCancelable(false)
                .setPositiveButton(R.string.camra,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton(R.string.galary,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(getFilesDir(), fileName);
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filepath = getPath(AddDriverActivity.this, data.getData());
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                      Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                     Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    filepath = getPath(this, getImageUri(this, resized));
                    if (image_type == 1) {
                        file_licence = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgLicence, "file://" + mFileTemp.getPath());
                        // imgLicence.setImageBitmap(bitmap);
                    } else if (image_type == 2) {
                        file_aadharcard = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgAadharCard, "file://" + mFileTemp.getPath());
                        //imgAadharCard.setImageBitmap(bitmap);
                    } else if (image_type == 3) {
                        file_photo = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgPhoto, "file://" + mFileTemp.getPath());
                        // imgPhoto.setImageBitmap(bitmap);
                    } else if (image_type == 4) {
                        file_police = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, imgPolice, "file://" + mFileTemp.getPath());
                        //imgPolice.setImageBitmap(bitmap);
                    }
                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
                // Utils.loadImageRoundSignup(this, imageView, "file://" + mFileTemp.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                filepath = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filepath = mFileTemp.getAbsolutePath();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                filepath = getPath(AddDriverActivity.this, getImageUri(AddDriverActivity.this, resized));
                if (image_type == 1) {
                    file_licence = getPath(AddDriverActivity.this, getImageUri(AddDriverActivity.this, resized));;
                    Utils.loadImage(AddDriverActivity.this, imgLicence, "file://" + mFileTemp.getPath());
                } else if (image_type == 2) {
                    file_aadharcard = getPath(AddDriverActivity.this, getImageUri(AddDriverActivity.this, resized));;
                    Utils.loadImage(AddDriverActivity.this, imgAadharCard, "file://" + mFileTemp.getPath());
                } else if (image_type == 3) {
                    file_photo = getPath(AddDriverActivity.this, getImageUri(AddDriverActivity.this, resized));
                    Utils.loadImage(AddDriverActivity.this, imgPhoto, "file://" + mFileTemp.getPath());
                } else if (image_type == 4) {
                    file_police = getPath(AddDriverActivity.this, getImageUri(AddDriverActivity.this, resized));;
                    Utils.loadImage(AddDriverActivity.this, imgPolice, "file://" + mFileTemp.getPath());
                }
                //  Utils.loadImageRoundSignup(AddMarketPlace.this, imageView, "file://" + mFileTemp.getPath());


            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        }

    }

    private void validateViews() {
        if (Utils.isConnectingToInternet(AddDriverActivity.this)) {
            try {
                enableLoadingBar(true);
                JSONObject obj = new JSONObject();
                obj.put("firstName", editName.getText().toString());
                obj.put("lastName", editLastName.getText().toString());
                obj.put("mobile", editMobile.getText().toString());
                obj.put("password", editPassword.getText().toString());
                obj.put("ln",MyApplication.getlangID(Constants.LangId));
                obj.put("address", addressJson);

                MultipartBody.Part part_police = null;

                final File file = new File(file_licence);
                RequestBody requestBody2 = RequestBody.create(MediaType.parse(getMimeType(file_licence)), file);
                MultipartBody.Part part_licence = MultipartBody.Part.createFormData("driverLicense", file.getName(), requestBody2);

                final File file1 = new File(file_aadharcard);
                RequestBody requestBody21 = RequestBody.create(MediaType.parse(getMimeType(file_aadharcard)), file1);
                MultipartBody.Part part_aadhar = MultipartBody.Part.createFormData("aadharCard", file1.getName(), requestBody21);

                final File file2 = new File(file_photo);
                RequestBody requestBody22 = RequestBody.create(MediaType.parse(getMimeType(file_photo)), file2);
                MultipartBody.Part part_photo = MultipartBody.Part.createFormData("profileImage", file2.getName(), requestBody22);
                File file3 = null;
                if (file_police.compareTo("") != 0) {
                    file3 = new File(file_police);
                    RequestBody requestBody23 = RequestBody.create(MediaType.parse(getMimeType(file_police)), file3);
                    part_police = MultipartBody.Part.createFormData("policeVerify", file3.getName(), requestBody23);
                }


                System.out.println("update json " + obj.toString());
                RequestBody data = RequestBody.create(MediaType.parse("text/plain"), obj.toString().trim());
                ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
                Log.d("auth", MyApplication.getUserType(Constants.userType));
                Call<JsonObject> call = getResponse.AddDriver(part_licence, part_aadhar, part_photo, part_police, data, MyApplication.getUserType(Constants.userType),MyApplication.getlangID(Constants.LangId));
                final File finalFile = file3;
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        dismissProgressBar();
                        if (response.isSuccessful()) {
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response.body().toString());
                                Log.d("response", obj.toString());
                                file.delete();
                                file1.delete();
                                file2.delete();
                                if (finalFile != null) {
                                    finalFile.delete();
                                }

                                Toast.makeText(AddDriverActivity.this, R.string.driver_added_succss, Toast.LENGTH_SHORT).show();
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //TestFragment.imageStringpath.clear();
                            dismissProgressBar();
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                System.out.println("error response " + jsonObject.toString());
                                Toast.makeText(AddDriverActivity.this, jsonObject.getJSONObject("error").optString("message"), Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        dismissProgressBar();
                        //  TestFragment.imageStringpath.clear();
                        Toast.makeText(AddDriverActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                        System.out.println("fail response.." + t.toString());
                        try {
                            JSONObject object = new JSONObject(t.toString());
                            Toast.makeText(AddDriverActivity.this, object.getJSONObject("error").optString("message") + "", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {

                        }
                        dismissProgressBar();
                    }
                });
            } catch (Exception e) {
                // TestFragment.imageStringpath.clear();
                System.out.println("Exception..." + e.toString());
                dismissProgressBar();
            }
        } else {
            Toast.makeText(AddDriverActivity.this, getResources().getString(R.string.network_error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
