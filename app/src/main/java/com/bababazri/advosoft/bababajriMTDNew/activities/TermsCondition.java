package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 9/6/2018.
 */

public class TermsCondition extends BaseActiivty {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.webview)
    WebView webview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_condition);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.term_and_condition);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // setTitle(getResources().getString(R.string.term_and_condition));

        MyApplication.setIslagChange(TermsCondition.this, true);
        webview.getSettings();
        webview.setBackgroundColor(Color.TRANSPARENT);
        if (MyApplication.getlangID(Constants.LangId).compareTo("hi") == 0) {
            webview.loadUrl("file:///android_asset/Terms_hindi.html");
        } else {
            webview.loadUrl("file:///android_asset/Terms.html");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
