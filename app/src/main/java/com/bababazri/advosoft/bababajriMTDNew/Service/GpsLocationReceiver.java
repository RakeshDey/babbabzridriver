package com.bababazri.advosoft.bababajriMTDNew.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.bababazri.advosoft.bababajriMTDNew.activities.DialogActivity;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by advosoft on 7/16/2018.
 */

public class GpsLocationReceiver extends BroadcastReceiver implements LocationListener {
    Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        ctx = context;
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            // react on GPS provider change action
            // Toast.makeText(context, intent.getAction(), Toast.LENGTH_SHORT).show();
            // Log.d("data_location", intent.getDataString());
            try {
                LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                    Log.i("About GPS", "GPS is Enabled in your devide");
                    // Toast.makeText(ctx,"enable",Toast.LENGTH_SHORT).show();
                } else {
                    //showAlert
                    context.startActivity(new Intent(context, DialogActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }

            } catch (Exception e) {
                Log.d("exception", e.toString());
            }

            Log.d("data_location", intent.getAction());
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {


    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            ctx.sendBroadcast(poke);
        }
    }
}