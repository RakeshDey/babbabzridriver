package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.DriverHistoryFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.DriverProfileFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.DriverRequestFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MapsFragment;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.util.BadgeDrawable;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/29/2018.
 */

public class DriverHome extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.home)
    ImageView home;
    @Bind(R.id.request_list)
    ImageView requestList;
    @Bind(R.id.history)
    ImageView history;
    @Bind(R.id.profile)
    ImageView profile;
    LayerDrawable icon;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.bottom_ll)
    LinearLayout bottomLl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_homepage);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        replaceFragment(R.id.container, new MapsFragment());
        checkGps();
        callapiBadge();
        callsubtypeObj();
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                home.setImageResource(R.drawable.home_onclick);
                requestList.setImageResource(R.drawable.request);
                history.setImageResource(R.drawable.history);
                profile.setImageResource(R.drawable.account);
                replaceFragment(R.id.container, new MapsFragment());

            }
        });
        requestList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                home.setImageResource(R.drawable.home);
                requestList.setImageResource(R.drawable.request_onclick);
                history.setImageResource(R.drawable.history);
                profile.setImageResource(R.drawable.account);
                replaceFragment(R.id.container, new DriverRequestFragment());

            }
        });
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                home.setImageResource(R.drawable.home);
                requestList.setImageResource(R.drawable.request);
                history.setImageResource(R.drawable.history_onclick);
                profile.setImageResource(R.drawable.account);
                replaceFragment(R.id.container, new DriverHistoryFragment());
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                home.setImageResource(R.drawable.home);
                requestList.setImageResource(R.drawable.request);
                history.setImageResource(R.drawable.history);
                profile.setImageResource(R.drawable.account_onclick);
                replaceFragment(R.id.container, new DriverProfileFragment());
            }
        });

    }

    private void callapiBadge() {
        HashMap objectNew = new HashMap();
        //  objectNew.put("userid", MyApplication.getUserID(Constants.UserID));
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(DriverHome.this, WebUrls.BASE_URL + WebUrls.Api_GetBadge, objectNew, DriverHome.this, RequestCode.CODE_GetBadge, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver_home, menu);
        MenuItem itemCart = menu.findItem(R.id.action_noti);
        icon = (LayerDrawable) itemCart.getIcon();
        setBadgeCount(this, icon, "0");
        return true;
    }

    @Override
    protected void onRestart() {
        callapiBadge();
        super.onRestart();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_noti:
                //Toast.makeText(this, "You have selected Bookmark Menu", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(DriverHome.this, NotificationListActivity.class));
                return true;
            case R.id.action_available:
                startActivity(new Intent(DriverHome.this, visivilityActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_GetBadge) {
            try {
                JSONObject object = new JSONObject(response);
                String badge = object.getJSONObject("success").getJSONObject("data").optInt("badgeCount") + "";
                setBadgeCount(this, icon, badge);

            } catch (Exception e) {

            }
        } else if (taskcode == RequestCode.CODE_GetSubobj) {
            MyApplication.setValueData(Constants.ValueData, response);

        }
    }

    private void checkGps() {
        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        Boolean isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Boolean isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (isGPSEnable == false) {
            startActivity(new Intent(DriverHome.this, DialogActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    private void callsubtypeObj() {
        HashMap objectNew = new HashMap();

        new WebTask(DriverHome.this, WebUrls.BASE_URL + WebUrls.Api_SubtypeObj, objectNew, DriverHome.this, RequestCode.CODE_GetSubobj, 0);
    }
}
