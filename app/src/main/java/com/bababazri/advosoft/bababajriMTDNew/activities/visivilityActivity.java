package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.Service.GPSTracker;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.views.Utils;
import com.bababazri.advosoft.bababajriMTDNew.webutility.ApiConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.AppConfig;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by advosoft on 6/18/2018.
 */

public class visivilityActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.sp_cat)
    Spinner spCat;
    @Bind(R.id.sp_sub_cat)
    Spinner spSubCat;
    @Bind(R.id.price)
    EditText price;
    @Bind(R.id.sp_unit)
    Spinner spUnit;
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    JSONObject addressJson = null;
    JSONObject location = null;
    String addressId;
    ArrayList<String> material = new ArrayList<>();
    ArrayList<ArrayList<String>> material_type = new ArrayList<>();
    ArrayList<ArrayList<String>> materia_unit = new ArrayList<>();
    ArrayList<ArrayList<JSONArray>> materia_subtype = new ArrayList<>();
    ArrayList<ArrayList<String>> materia_type_id = new ArrayList<>();
    ArrayList<ArrayList<String>> materia_unit_default = new ArrayList<>();
    ArrayList<String> material_id = new ArrayList<>();
    Boolean data_check = false;
    String materialId = "", materialTypeId = "", unit = "";
    int pos = 0;
    @Bind(R.id.noti_toggle)
    ToggleButton notiToggle;
    @Bind(R.id.quantity)
    EditText quantity;
    @Bind(R.id.loaded_veh_photo)
    ImageView loadedVehPhoto;
    @Bind(R.id.bill_photo)
    ImageView billPhoto;

    String filepath = null;
    public static File mFileTemp;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x52;
    public static final int REQUEST_CODE_GALLERY = 0x51;
    int image_type = 0;
    String loaded_file = null, bill_file = null;
    int counthu = 0;
    @Bind(R.id.sp_sub_type)
    Spinner spSubType;
    @Bind(R.id.sub_type_ll)
    LinearLayout subTypeLl;
    ArrayList<String> value_key = new ArrayList<>();
    ArrayList<String> value_data = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visibility);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.add_product);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

         /*new value code*/
        value_key.clear();
        value_data.clear();
        try {
            JSONObject object_data = new JSONObject(MyApplication.getValueData(Constants.ValueData));
            JSONObject datata = object_data.getJSONObject("success").getJSONObject("data");

            Iterator<String> iter = datata.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    String value = datata.optString(key);
                    value_key.add(key);
                    value_data.add(value);
                } catch (Exception e) {
                    // Something went wrong!
                }
            }

        } catch (Exception e) {

        }

        callgetMaterial();
        callgetAvaibility();


       /* spCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                pos = i;
                if (data_check == false) {
                    if (i != 0) {
                        materialId = material_id.get(0);
                        data_check = true;
                        if (material_type.get(i) != null) {
                            if (material_type.get(i).size() != 0) {
                                ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, material_type.get(i));
                                spSubCat.setAdapter(adapter_1);
                            }
                        }
                        if (materia_unit.get(i) != null) {
                            if (materia_unit.get(i).size() != 0) {
                                ArrayAdapter<String> adapter__2 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, materia_unit.get(i));
                                spUnit.setAdapter(adapter__2);
                            }
                        }
                    }
                } else {
                    materialId = material_id.get(i);
                    if (material_type.get(i) != null) {
                        if (material_type.get(i).size() != 0) {
                            ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, material_type.get(i));
                            spSubCat.setAdapter(adapter_1);
                        }
                    }
                    if (materia_unit.get(i) != null) {
                        if (materia_unit.get(i).size() != 0) {
                            ArrayAdapter<String> adapter__2 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, materia_unit.get(i));
                            spUnit.setAdapter(adapter__2);
                        }
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spSubCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (data_check==false){
                    if (materialTypeId.length()!=0){
                        materialTypeId = materia_type_id.get(0).get(i);
                    }

                    Log.d("materials_pos",i+"");
                    Log.d("materials_size",materialTypeId.length()+"");

                }else {
                    materialTypeId = materia_type_id.get(pos).get(i);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                unit = spUnit.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantity.getText().toString().isEmpty()) {
                    quantity.setError(getString(R.string.notempty));
                } else if (price.getText().toString().isEmpty()) {
                    price.setError(getString(R.string.notempty));
                } else {
                    calladdProduct();
                }

            }
        });

        loadedVehPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 1;
                uploadImageTypePost();
            }
        });
        billPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_type = 2;
                uploadImageTypePost();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callgetMaterial() {
        HashMap objectNew = new HashMap();

        new WebTask(visivilityActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetMaterial, objectNew, visivilityActivity.this, RequestCode.CODE_GetMaterial, 0);
    }

    private void callgetAvaibility() {
        HashMap objectNew = new HashMap();

        new WebTask(visivilityActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetAvailbale, objectNew, visivilityActivity.this, RequestCode.CODE_GetAvailable, 0);
    }


    private void calladdProduct() {
        Double lat = 0.0, lng = 0.0;
        GPSTracker tracker = new GPSTracker(visivilityActivity.this);
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            lat = tracker.getLatitude();
            lng = tracker.getLongitude();
        }
        try {
            location = new JSONObject();
            location.put("lat", Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)));
            location.put("lng", Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG)));
        } catch (Exception e) {

        }
        if (location != null) {
            validateViews();
        } else {
            Toast.makeText(visivilityActivity.this, R.string.pls_turn_gps, Toast.LENGTH_SHORT).show();
        }

/*
        HashMap objectNew = new HashMap();
        objectNew.put("materialId", materialId);
        objectNew.put("materialTypeId", materialTypeId);
        objectNew.put("location", location);
        objectNew.put("isAvailable", notiToggle.isChecked() + "");
        objectNew.put("quantity", quantity.getText().toString());
        //objectNew.put("address", addressJson + "");
        objectNew.put("unit", unit);
        objectNew.put("price", price.getText().toString());
        new WebTask(visivilityActivity.this, WebUrls.BASE_URL + WebUrls.Api_DriverAvail, objectNew, visivilityActivity.this, RequestCode.CODE_AddProduct, 1);*/
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_GetMaterial) {
            try {
                JSONObject object = new JSONObject(response);
                JSONObject success_obj = object.getJSONObject("success");
                JSONArray jsonArray = success_obj.getJSONArray("data");
                JSONObject obj_content = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    obj_content = jsonArray.getJSONObject(i);
                    material.add(obj_content.optString("name"));
                    material_id.add(obj_content.optString("id"));
                    JSONArray jsonArray1 = obj_content.getJSONArray("materialTypes");
                    ArrayList<String> material_subname = new ArrayList<>();
                    ArrayList<JSONArray> material_subtype = new ArrayList<>();
                    ArrayList<String> material_submaterial_id = new ArrayList<>();
                    JSONObject submaterial_obj = null;
                    material_subname.clear();
                    material_submaterial_id.clear();
                    material_subtype.clear();
                    for (int j = 0; j < jsonArray1.length(); j++) {

                        submaterial_obj = jsonArray1.getJSONObject(j);
                        material_subname.add(submaterial_obj.optString("name"));
                        material_submaterial_id.add(submaterial_obj.optString("id"));
                        material_subtype.add(submaterial_obj.optJSONArray("subTypes"));
                    }
                    material_type.add(material_subname);
                    materia_type_id.add(material_submaterial_id);
                    materia_subtype.add(material_subtype);

                    JSONArray jsonArray2 = obj_content.getJSONArray("units");
                    ArrayList<String> unit_array = new ArrayList<>();
                    ArrayList<String> unit_array_default = new ArrayList<>();
                    unit_array.clear();
                    for (int k = 0; k < jsonArray2.length(); k++) {
                        String key = jsonArray2.getString(k);
                        int resID = getResources().getIdentifier(key, "string", getPackageName());
                        unit_array.add(getString(resID));
                        unit_array_default.add(key);
                    }
                    materia_unit.add(unit_array);
                    materia_unit_default.add(unit_array_default);

                }
                setAdapter();

            } catch (Exception e) {

            }
        } else if (taskcode == RequestCode.CODE_AddProduct) {
            Toast.makeText(visivilityActivity.this, R.string.product_added_success, Toast.LENGTH_SHORT).show();

        } else if (taskcode == RequestCode.CODE_GetAvailable) {
            try {
                UpdateUI(response);

            } catch (Exception e) {

            }
        }

    }

    private void setAdapter() {
        ArrayAdapter<String> adapter_new = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, material);
        ArrayAdapter<String> adapter_1_new = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, material_type.get(0));
        ArrayAdapter<String> adapter__2_new = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, materia_unit.get(0));

        spSubCat.setAdapter(adapter_1_new);
        spUnit.setAdapter(adapter__2_new);
        spCat.setAdapter(adapter_new);

        try {
            JSONArray arr_sub = materia_subtype.get(0).get(0);
            ArrayList<String> arr_str = new ArrayList<>();
            JSONObject object_data = new JSONObject(MyApplication.getValueData(Constants.ValueData));
            for (int i = 0; i < arr_sub.length(); i++) {
                arr_str.add(object_data.getJSONObject("success").getJSONObject("data").optString(arr_sub.getString(i).toLowerCase()));
            }
            ArrayAdapter<String> adapter__sub_type = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr_str);
            spSubType.setAdapter(adapter__sub_type);
        } catch (Exception e) {

        }


        setclickListner();


    }

    private void setclickListner() {
        spCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                pos = i;
                materialId = material_id.get(i);
                counthu++;
                if (counthu != 1) {
                    if (material_type.get(i) != null) {
                        if (material_type.get(i).size() != 0) {
                            ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(visivilityActivity.this, android.R.layout.simple_spinner_item, material_type.get(i));
                            spSubCat.setAdapter(adapter_1);
                        }
                    }
                    if (materia_unit.get(i) != null) {
                        if (materia_unit.get(i).size() != 0) {
                            ArrayAdapter<String> adapter__2 = new ArrayAdapter<String>(visivilityActivity.this, android.R.layout.simple_spinner_item, materia_unit.get(i));
                            spUnit.setAdapter(adapter__2);
                        }
                    }


                }

               /* if (data_check == false) {
                    if (i != 0) {
                        materialId = material_id.get(0);
                        data_check = true;
                        if (material_type.get(i) != null) {
                            if (material_type.get(i).size() != 0) {
                                ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, material_type.get(i));
                                spSubCat.setAdapter(adapter_1);
                            }
                        }
                        if (materia_unit.get(i) != null) {
                            if (materia_unit.get(i).size() != 0) {
                                ArrayAdapter<String> adapter__2 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, materia_unit.get(i));
                                spUnit.setAdapter(adapter__2);
                            }
                        }
                    }
                } else {
                    materialId = material_id.get(i);
                    if (material_type.get(i) != null) {
                        if (material_type.get(i).size() != 0) {
                            ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, material_type.get(i));
                            spSubCat.setAdapter(adapter_1);
                        }
                    }
                    if (materia_unit.get(i) != null) {
                        if (materia_unit.get(i).size() != 0) {
                            ArrayAdapter<String> adapter__2 = new ArrayAdapter<String>(AddProductActivity.this, android.R.layout.simple_spinner_item, materia_unit.get(i));
                            spUnit.setAdapter(adapter__2);
                        }
                    }
                }*/


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spSubCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                materialTypeId = materia_type_id.get(pos).get(i);
                if (materia_subtype.get(pos) != null) {
                    if (materia_subtype.get(pos).size() != 0) {
                        try {
                            JSONArray arr_sub = materia_subtype.get(pos).get(i);
                            ArrayList<String> arr_str = new ArrayList<>();
                            if (arr_sub == null || arr_sub.length() == 0) {
                                spSubType.setVisibility(View.GONE);
                                subTypeLl.setVisibility(View.GONE);
                            } else {
                                spSubType.setVisibility(View.VISIBLE);
                                subTypeLl.setVisibility(View.VISIBLE);
                            }
                            JSONObject object_data = new JSONObject(MyApplication.getValueData(Constants.ValueData));
                            for (int j = 0; j < arr_sub.length(); j++) {
                                //arr_str.add(arr_sub.getString(j));
                                arr_str.add(object_data.getJSONObject("success").getJSONObject("data").optString(arr_sub.getString(j).toLowerCase()));
                            }
                            if (counthu != 1) {
                                ArrayAdapter<String> adapter__sub_type = new ArrayAdapter<String>(visivilityActivity.this, android.R.layout.simple_spinner_item, arr_str);
                                spSubType.setAdapter(adapter__sub_type);
                            }
                        } catch (Exception e) {

                        }
                    }

                }

               /* if (data_check==false){
                    if (materialTypeId.length()!=0){
                        materialTypeId = materia_type_id.get(0).get(i);
                    }

                    Log.d("materials_pos",i+"");
                    Log.d("materials_size",materialTypeId.length()+"");

                }else {
                    materialTypeId = materia_type_id.get(pos).get(i);
                }*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                unit = materia_unit_default.get(pos).get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void UpdateUI(String response) {
        try {
            JSONObject object = new JSONObject(response);
            JSONObject data_obj = object.getJSONObject("success").getJSONObject("data");
            notiToggle.setChecked(data_obj.optBoolean("isAvailable"));
            price.setText(data_obj.optString("price"));
            quantity.setText(data_obj.optInt("orderQuantity") + "");
            if (data_obj.optString("loadingVehicleImg") != null) {
                if (!data_obj.optString("loadingVehicleImg").isEmpty()) {
                    Utils.loadImage(visivilityActivity.this, loadedVehPhoto, WebUrls.BASE_URL + data_obj.optString("loadingVehicleImg"));
                }

            }
            if (data_obj.optString("billImg") != null) {
                if (!data_obj.optString("billImg").isEmpty()) {
                    Utils.loadImage(visivilityActivity.this, billPhoto, WebUrls.BASE_URL + data_obj.optString("billImg"));
                }

            }

            int index_cat = material_id.indexOf(data_obj.optString("materialId"));
            spCat.setSelection(index_cat);

            ArrayAdapter<String> adapter_1_new = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, material_type.get(index_cat));
            spSubCat.setAdapter(adapter_1_new);

            ArrayAdapter<String> adapter__2_new = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, materia_unit.get(index_cat));
            spUnit.setAdapter(adapter__2_new);
            Log.d("subcatindex", materia_type_id.get(index_cat).indexOf(data_obj.optString("materialTypeId")) + "");

            spSubCat.setSelection(materia_type_id.get(index_cat).indexOf(data_obj.optString("materialTypeId")));
            JSONObject object_data = new JSONObject(MyApplication.getValueData(Constants.ValueData));
            JSONObject datata = object_data.getJSONObject("success").getJSONObject("data");
            Log.d("index_unit", materia_unit.get(index_cat).indexOf(datata.optString(data_obj.optString("unit").toLowerCase())) + "");

            spUnit.setSelection(materia_unit.get(index_cat).indexOf(datata.optString(data_obj.optString("unit").toLowerCase())));

            try {
                int sub_index = materia_type_id.get(index_cat).indexOf(data_obj.optString("materialTypeId"));
                JSONArray arr_sub = materia_subtype.get(index_cat).get(sub_index);
                ArrayList<String> arr_str = new ArrayList<>();
                if (arr_sub == null || arr_sub.length() == 0) {
                    spSubType.setVisibility(View.GONE);
                    subTypeLl.setVisibility(View.GONE);
                } else {
                    spSubType.setVisibility(View.VISIBLE);
                    subTypeLl.setVisibility(View.VISIBLE);
                }

                for (int j = 0; j < arr_sub.length(); j++) {
                    //arr_str.add(arr_sub.getString(j));
                    arr_str.add(object_data.getJSONObject("success").getJSONObject("data").optString(arr_sub.getString(j).toLowerCase()));
                }
                ArrayAdapter<String> adapter__sub_type = new ArrayAdapter<String>(visivilityActivity.this, android.R.layout.simple_spinner_item, arr_str);
                spSubType.setAdapter(adapter__sub_type);
                Log.d("index_subtype", arr_str.indexOf(datata.optString(data_obj.optString("subType").toLowerCase())) + "");
                spSubType.setSelection(arr_str.indexOf(datata.optString(data_obj.optString("subType").toLowerCase())));
            } catch (Exception e) {

            }


        } catch (Exception e) {
            Log.d("exception...",e.toString());
            //counthu=3;

        }

    }

    public void uploadImageTypePost() {
        setimagepath();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(R.string.choose_option);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.select_img_from)
                .setCancelable(false)
                .setPositiveButton(R.string.camra,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                takePicture();

                            }

                        })
                .setNegativeButton(R.string.galary,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                openGallery();
                            }

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setCancelable(true);
        // show it
        alertDialog.show();
    }

    private void setimagepath() {

        String fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdIconStorageDir = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/");
            // create storage directories, if they don't exist
            sdIconStorageDir.mkdirs();

            mFileTemp = new File(Environment.getExternalStorageDirectory() + "/" + "Pictures/", fileName);
        } else {
            mFileTemp = new File(getFilesDir(), fileName);
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
            Log.d("", "cannot take picture", e);
        }
    }

    public static String getPath(Context context, Uri uri) {
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY) {
            try {
                System.out.println("on activity result gallery");
                filepath = getPath(visivilityActivity.this, data.getData());
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                    System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                    if (image_type == 1) {
                        loaded_file = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, loadedVehPhoto, "file://" + mFileTemp.getPath());
                    } else if (image_type == 2) {
                        bill_file = getPath(this, getImageUri(this, resized));
                        Utils.loadImage(this, billPhoto, "file://" + mFileTemp.getPath());
                    }


                } catch (Exception e) {
                    System.out.println("exception...." + e.toString());
                }
                // Utils.loadImageRoundSignup(this, imageView, "file://" + mFileTemp.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CODE_TAKE_PICTURE) {
            try {
                filepath = Uri.fromFile(new File(mFileTemp.getAbsolutePath())).toString();
                filepath = mFileTemp.getAbsolutePath();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(mFileTemp.getAbsolutePath())));
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 400, 400, true);
                System.out.println("image width" + resized.getWidth() + " image height" + resized.getHeight());
                if (image_type == 1) {
                    loaded_file = getPath(visivilityActivity.this, getImageUri(visivilityActivity.this, resized));
                    Utils.loadImage(this, loadedVehPhoto, "file://" + mFileTemp.getPath());
                } else if (image_type == 2) {
                    bill_file = mFileTemp.getAbsolutePath();
                    Utils.loadImage(this, billPhoto, "file://" + mFileTemp.getPath());
                }


                //  Utils.loadImageRoundSignup(AddMarketPlace.this, imageView, "file://" + mFileTemp.getPath());


            } catch (Exception e) {
                System.out.println("exception...." + e.toString());
            }
        }

    }

    private void validateViews() {
        if (Utils.isConnectingToInternet(visivilityActivity.this)) {
            try {

                enableLoadingBar(true);
                JSONObject obj = new JSONObject();
                obj.put("materialId", materialId);
                obj.put("materialTypeId", materialTypeId);
                obj.put("location", location);
                obj.put("isAvailable", notiToggle.isChecked());
                obj.put("quantity", quantity.getText().toString());
                obj.put("unit", unit);
                if (subTypeLl.getVisibility() == View.VISIBLE) {
                    String subkey = value_key.get(value_data.indexOf(spSubType.getSelectedItem().toString()));
                    obj.put("subType", subkey);
                }
                obj.put("price", price.getText().toString());

                MultipartBody.Part part_vehical = null, part_bill = null;
                if (loaded_file != null) {
                    File file = new File(loaded_file);
                    RequestBody requestBody2 = RequestBody.create(MediaType.parse(getMimeType(loaded_file)), file);
                    part_vehical = MultipartBody.Part.createFormData("loadingVehicleImg", file.getName(), requestBody2);
                }
                if (bill_file != null) {
                    File file = new File(bill_file);
                    RequestBody requestBody2 = RequestBody.create(MediaType.parse(getMimeType(bill_file)), file);
                    part_bill = MultipartBody.Part.createFormData("billImg", file.getName(), requestBody2);
                }


                System.out.println("update json " + obj.toString());
                RequestBody data = RequestBody.create(MediaType.parse("text/plain"), obj.toString().trim());
                ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
                Log.d("auth", MyApplication.getUserType(Constants.userType));
                Call<JsonObject> call = getResponse.DriverVisibility(part_vehical, part_bill, data, MyApplication.getUserType(Constants.userType),MyApplication.getlangID(Constants.LangId));
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        dismissProgressBar();
                        if (response.isSuccessful()) {
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response.body().toString());
                                Log.d("response", obj.toString());
                                Toast.makeText(visivilityActivity.this, R.string.product_added_success, Toast.LENGTH_SHORT).show();
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //TestFragment.imageStringpath.clear();
                            dismissProgressBar();
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                System.out.println("error response " + jsonObject.getJSONObject("error").optString("message"));
                                //Toast.makeText(visivilityActivity.this, jsonObject.getJSONObject("error").optString("message").toString(), Toast.LENGTH_LONG).show();
                                String msg = jsonObject.getJSONObject("error").optString("message");

                                // Toast.makeText(visivilityActivity.this, "datatytytd", Toast.LENGTH_SHORT).show();

                                Toast.makeText(visivilityActivity.this, jsonObject.getJSONObject("error").optString("message").toString(), Toast.LENGTH_LONG).show();

                            } catch (Exception e) {
                                Log.d("exception..", e.toString());

                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        dismissProgressBar();
                        //  TestFragment.imageStringpath.clear();
                        Toast.makeText(visivilityActivity.this, "" + t.toString(), Toast.LENGTH_SHORT).show();
                        System.out.println("fail response.." + t.toString());
                        try {
                            JSONObject object = new JSONObject(t.toString());
                            Toast.makeText(visivilityActivity.this, object.getJSONObject("error").optString("message") + "", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {

                        }
                        dismissProgressBar();
                    }
                });
            } catch (Exception e) {
                // TestFragment.imageStringpath.clear();
                System.out.println("Exception..." + e.toString());
                dismissProgressBar();
            }
        } else {
            Toast.makeText(visivilityActivity.this, getResources().getString(R.string.network_error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
