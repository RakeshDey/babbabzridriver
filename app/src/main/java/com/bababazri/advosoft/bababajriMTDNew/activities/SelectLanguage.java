package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 10/29/2018.
 */

public class SelectLanguage extends BaseActiivty {
    @Bind(R.id.select_lang_tv)
    TextView selectLangTv;
    @Bind(R.id.english_tv_select_lang)
    TextView englishTvSelectLang;
    @Bind(R.id.hindi_tv_select_lang)
    TextView hindiTvSelectLang;
    @Bind(R.id.box_1)
    LinearLayout box1;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        englishTvSelectLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLangRecreate("en");
            }
        });
        hindiTvSelectLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLangRecreate("hi");
            }
        });
    }

    public void setLangRecreate(String langval) {
        Locale locale;
        Configuration config = getResources().getConfiguration();
        locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        MyApplication.setLangId(Constants.LangId, langval);

        Intent intent = new Intent(SelectLanguage.this, LoginActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        //   MyApplication.setIslagChange(ProfileActivity.this, true);
        // recreate();

    }
}
