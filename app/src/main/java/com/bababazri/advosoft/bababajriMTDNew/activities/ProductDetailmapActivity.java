package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.animation.ValueAnimator;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.util.DirectionsJSONParser;
import com.bababazri.advosoft.bababajriMTDNew.util.ParserTask;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.RequestListWrapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/25/2018.
 */

public class ProductDetailmapActivity extends BaseActiivty implements OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, ParserTask.ParserCallback, WebCompleteTask {
    ArrayList markerPoints = new ArrayList();
    LatLng source = new LatLng(0, 0);
    LatLng destination = new LatLng(0, 0);
    String sourceAddress, destinationAddress;
    Polyline polyline;
    int counter = 2;
    static Marker sourceMarker, carMarker;
    List<HashMap<String, String>> routePoints;
    @Bind(R.id.order_msg)
    TextView orderMsg;
    @Bind(R.id.msg_ll)
    LinearLayout msgLl;
    static private GoogleMap mMap;
    RequestListWrapper feed;
    GoogleApiClient googleApiClient = null;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.agent_feedback)
    TextView agentFeedback;

   /* BroadcastReceiver locationUpdated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location newLocation = intent.getParcelableExtra("location");

            String lat = String.valueOf(newLocation.getLatitude());
            String lng = String.valueOf(newLocation.getLongitude());
            if (!MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT).equalsIgnoreCase(lat) ||
                    !MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG).equalsIgnoreCase(lng)) {

                Location location = new Location("");
                location.setLatitude(Double.parseDouble(lat));
                location.setLongitude(Double.parseDouble(lng));

                if (TextUtils.isEmpty(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT))) {
                    *//*location.setBearing(bearingBetweenLocations(new LatLng(model.getSource().getLocation().getLat(), (model.getSource().getLocation().getLng())),
                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));*//*
                    location.setBearing(bearingBetweenLocations(new LatLng(26.805085, 75.812576),
                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                } else {
                    location.setBearing(bearingBetweenLocations(new LatLng(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)), Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG))),
                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                }
                animateMarker(location, carMarker);
                MyApplication.setSharedPrefString(Constants.SP_CURRENT_LAT, lat);
                MyApplication.setSharedPrefString(Constants.SP_CURRENT_LONG, lng);
            }
        }
    };*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_list_booking_activity);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        feed = (RequestListWrapper) getIntent().getSerializableExtra("data");
        //startService(new Intent(ProductDetailmapActivity.this, LocationTracker.class));

        agentFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMethod();
            }
        });

        if (feed.getAgent_feedbackstatus().compareTo("pending")!=0){
            agentFeedback.setVisibility(View.VISIBLE);
        }else {
            agentFeedback.setVisibility(View.GONE);
        }

        setMap();
        //connectToGoogleApi();
        try {
            JSONObject object = new JSONObject(feed.getSrc_location());
            JSONObject des_obj = new JSONObject(feed.getLatlng());
            Location location = new Location("");
            location.setLatitude(object.optDouble("lat"));
            location.setLongitude(object.optDouble("lng"));
            source = new LatLng(object.optDouble("lat"), object.optDouble("lng"));
            destination = new LatLng(des_obj.optDouble("lat"), des_obj.optDouble("lng"));
            animateMarker(location, carMarker);
        } catch (Exception e) {

        }
        //  getRoute();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Locations").child(feed.getId());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    try {
                        Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                        Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));
                        JSONObject jsonObject = new JSONObject(value1);
                        if (jsonObject.optString("jobStatus").compareTo("pending") == 0) {
                            Toast.makeText(ProductDetailmapActivity.this, R.string.product_not_dispatched, Toast.LENGTH_SHORT).show();
                            msgLl.setVisibility(View.VISIBLE);
                            orderMsg.setVisibility(View.VISIBLE);
                            orderMsg.setText(R.string.oredr_not_dispathched);
                        } else if (jsonObject.optString("jobStatus").compareTo("start") == 0) {
                            msgLl.setVisibility(View.GONE);
                            orderMsg.setVisibility(View.GONE);
                            String lat = String.valueOf(jsonObject.optString("currntLatt"));
                            String lng = String.valueOf(jsonObject.optString("currntLong"));
                            if (!MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT).equalsIgnoreCase(lat) ||
                                    !MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG).equalsIgnoreCase(lng)) {

                                Location location = new Location("");
                                location.setLatitude(Double.parseDouble(lat));
                                location.setLongitude(Double.parseDouble(lng));

                                if (TextUtils.isEmpty(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT))) {
                                    //  location.setBearing(bearingBetweenLocations(new LatLng(model.getSource().getLocation().getLat(), (model.getSource().getLocation().getLng())),
                                    //        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                                    location.setBearing(bearingBetweenLocations(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)),
                                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                                } else {

                                    Location location1 = new Location("");
                                    location1.setLatitude(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)));
                                    location1.setLongitude(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG)));
                                    float bea = location1.bearingTo(location);
                                    location.setBearing(bea);
                                }
                                animateMarker(location, carMarker);
                                MyApplication.setSharedPrefString(Constants.SP_CURRENT_LAT, lat);
                                MyApplication.setSharedPrefString(Constants.SP_CURRENT_LONG, lng);
                            }

                        } else if (jsonObject.optString("jobStatus").compareTo("end") == 0) {
                            msgLl.setVisibility(View.VISIBLE);
                            orderMsg.setVisibility(View.VISIBLE);
                            orderMsg.setText(R.string.order_complete);
                        }
                    } catch (Exception e) {
                        Log.d("exception...", e.toString());
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void setMap() {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        mapFragment.getMapAsync(ProductDetailmapActivity.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }

        // Adding new item to the ArrayList
        markerPoints.add(source);
        markerPoints.add(destination);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(source);
        options.position(destination);

        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }


        sourceMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));
        carMarker = mMap.addMarker(new MarkerOptions().position(source)
                .flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_truck))
                .title(sourceAddress));

        mMap.addMarker(new MarkerOptions().position(destination)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(destinationAddress));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        // LocalBroadcastManager.getInstance(this).registerReceiver(locationUpdated, new IntentFilter("locationUpdated"));
    }

    @Override
    public void onMapLoaded() {

    }

    @Override
    public void onParserResult(List<List<HashMap<String, String>>> result, DirectionsJSONParser parser, String status) {
        for (int i = 0; i < parser.getRouteList().size(); i++) {
            /*if (parser.getRouteList().get(i).getSelectedRoute().equalsIgnoreCase(model.getSelectedRoute())) {
                routePoints = result.get(i);
                showRoute(routePoints);
            }*/
            routePoints = result.get(i);
            Log.d("routePoints", routePoints + "");
            showRoute(routePoints);

        }


    }


    void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            /*final LatLng startPosition = carMarker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = carMarker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 0.5f);
            valueAnimator.setDuration(1000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        carMarker.setPosition(newPosition);
                       // carMarker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                        carMarker.setRotation(bearingBetweenLocations(startPosition,new LatLng(destination.getLatitude(),destination.getLongitude())));

                    } catch (Exception ex) {
                    }
                }

            });
            valueAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(carMarker.getPosition(), 18.0f));
//                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18.0f));
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            valueAnimator.start();
            counter++;*/


            /*new code*/
            final LatLng startPosition = carMarker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());


            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(2000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float v = valueAnimator.getAnimatedFraction();
                    double lng = v * endPosition.longitude + (1 - v)
                            * startPosition.longitude;
                    double lat = v * endPosition.latitude + (1 - v)
                            * startPosition.latitude;

                    LatLng newPos = new LatLng(lat, lng);
                    Location location1 = new Location("");
                    location1.setLatitude(startPosition.latitude);
                    location1.setLongitude(startPosition.longitude);


                    Location location2 = new Location("");
                    location2.setLatitude(newPos.latitude);
                    location2.setLongitude(newPos.longitude);
                    float bear = location1.bearingTo(location2);
                    Log.d("distance", location1.distanceTo(location2) + "");
                    Log.d("accuracy", location2.getAccuracy() + "");
                    Log.d("total", location1.distanceTo(location2) - location2.getAccuracy() + "");
                    Log.d("update_bearing", bear + "");

                    if (location1.distanceTo(location2) - location2.getAccuracy() > 8) {
                        carMarker.setPosition(newPos);
                        carMarker.setAnchor(0.5f, 0.5f);

                        carMarker.setRotation(bear);

                        if (mMap != null) {
                            mMap.moveCamera(CameraUpdateFactory
                                    .newCameraPosition
                                            (new CameraPosition.Builder()
                                                    .target(newPos)
                                                    .zoom(18.5f)
                                                    .build()));
                        }
                    }
                    /*marker.setPosition(newPos);
                    marker.setAnchor(0.5f, 0.5f);
                    marker.setRotation(getBearing(startPosition, newPos));
                    mMap.moveCamera(CameraUpdateFactory
                            .newCameraPosition
                                    (new CameraPosition.Builder()
                                            .target(newPos)
                                            .zoom(18.5f)
                                            .build()));*/
                }
            });
            valueAnimator.start();
        }
    }

    float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }


    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    private float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        Double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng.floatValue();
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void getRoute() {
        String url = MyApplication.getDirectionsUrl(source, destination);

        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(this, url, objectNew, ProductDetailmapActivity.this, RequestCode.CODE_MyVehical, 0);

       /* ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                ParserTask parserTask = new ParserTask(this);
                parserTask.execute(response.toString());
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });*/
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_MyVehical) {
            if (response != null) {

                ParserTask parserTask = new ParserTask(this, "start");
                parserTask.execute(response.toString());
            }
        }

    }

    private void showRoute(List<HashMap<String, String>> data) {

        if (polyline != null) {
            polyline.remove();
        }

        ArrayList points = null;
        PolylineOptions lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        points = new ArrayList();
        lineOptions = new PolylineOptions();

        for (int j = 0; j < data.size(); j++) {
            HashMap<String, String> point = data.get(j);

            double lat = Double.parseDouble(point.get("lat").toString());
            double lng = Double.parseDouble(point.get("lng").toString());

            LatLng position = new LatLng(lat, lng);

            points.add(position);
//            builder.include(position);
        }

        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(getResources().getColor(R.color.colorPrimary));
        lineOptions.geodesic(true);
// Drawing polyline in the Google Map for the i-th route
        polyline = mMap.addPolyline(lineOptions);
//        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12);


//        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//        mMap.animateCamera(cu);
    }

    private void connectToGoogleApi() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            Log.d("locat", "connected to googleapiclient");
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            Log.d("locat", "suspended connection to googleapiclient");
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult result) {
                            Log.d("locat", "failed to cennect - " + result.getErrorMessage());
                        }
                    })
                    .addApi(LocationServices.API)
                    .build();
        }
    }


    public void popupMethod(){
      final AlertDialog  alertDialog = new AlertDialog.Builder(ProductDetailmapActivity.this).create();
        View view = getLayoutInflater().inflate(R.layout.feedback_dialog,null);
        ImageView clear_img=(ImageView)view.findViewById(R.id.clear_img);
        TextView text_feedback=(TextView)view.findViewById(R.id.text_feed_back);
        TextView veri_status=(TextView)view.findViewById(R.id.status);
        clear_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        if (feed.getAgent_feedbackstatus().compareTo("true")==0){
            veri_status.setText(getResources().getString(R.string.verification_status)+" "+getResources().getString(R.string.approve_txt));
        }else {
            veri_status.setText(getResources().getString(R.string.verification_status)+" "+getResources().getString(R.string.disapproved));
        }
       // veri_status.setText(getResources().getString(R.string.verification_status)+" "+feed.getAgent_feedbackstatus());
        text_feedback.setText(feed.getAgent_feedback());
        alertDialog.setView(view);
        alertDialog.show();
    }

}
