package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/15/2018.
 */

public class RegistrationActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.first_name)
    EditText firstName;
    @Bind(R.id.last_name)
    EditText lastName;
    @Bind(R.id.phone_number)
    EditText phoneNumber;
    @Bind(R.id.address)
    PlacesAutocompleteTextView address;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.confirm_password)
    EditText confirmPassword;
    @Bind(R.id.sp_login_as)
    Spinner spLoginAs;
    @Bind(R.id.ll)
    LinearLayout ll;
    String realmis = "";
    @Bind(R.id.btn_submit)
    Button btnSubmit;
    PlaceDetails addressplace;
    JSONObject addressJson = null;
    String addressId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_2);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.registration));
        address.setLanguageCode(MyApplication.getlangID(Constants.LangId));

        spLoginAs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (position==1){
                        realmis="owner";
                    }else if (position==2){
                        realmis="agent";
                    }
                   // realmis = spLoginAs.getSelectedItem().toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        address.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull final Place place) {
                addressId = place.place_id;
//                MyApplication.showProgressDialog("Getting Location");
                address.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                        addressplace = placeDetails;

                        addressJson = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            addressJson.put("address", placeDetails.formatted_address);
                            addressJson.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                    }
                });
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSubmitTask();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callSubmitTask() {

        if (TextUtils.isEmpty(firstName.getText().toString())) {
            firstName.setError(getString(R.string.notempty));
            firstName.requestFocus();
        } else if (TextUtils.isEmpty(lastName.getText().toString())) {
            lastName.setError(getString(R.string.notempty));
            lastName.requestFocus();
        } else if (TextUtils.isEmpty(phoneNumber.getText().toString())) {
            phoneNumber.setError(getString(R.string.notempty));
            phoneNumber.requestFocus();
        } else if (phoneNumber.getText().toString().length() < 10) {
            Toast.makeText(RegistrationActivity.this, R.string.mobile_not_valid, Toast.LENGTH_SHORT).show();
        } else if (addressJson == null) {
            address.setError(getString(R.string.notempty));
            address.requestFocus();
        } else if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError(getString(R.string.notempty));
            password.requestFocus();
        }else if (password.getText().length()<6){
            password.setError(getString(R.string.password_shdnot_less));
            password.requestFocus();
        }else if (TextUtils.isEmpty(confirmPassword.getText().toString())) {
            confirmPassword.setError(getString(R.string.notempty));
            confirmPassword.requestFocus();
        } else if (password.getText().toString().compareTo(confirmPassword.getText().toString()) != 0) {
            Toast.makeText(RegistrationActivity.this, R.string.password_not_matched, Toast.LENGTH_SHORT).show();
        } else if (realmis.compareTo("") == 0) {
            Toast.makeText(RegistrationActivity.this, R.string.select_registration_type, Toast.LENGTH_SHORT).show();
            spLoginAs.requestFocus();
        } else {
            HashMap objectNew = new HashMap();
            objectNew.put("firstName", firstName.getText().toString());
            objectNew.put("lastName", lastName.getText().toString());
            objectNew.put("address", addressJson + "");
            objectNew.put("realm", realmis.toLowerCase());
            objectNew.put("mobile", phoneNumber.getText().toString());
            objectNew.put("password", password.getText().toString());
            objectNew.put("ln", MyApplication.getlangID(Constants.LangId));
            new WebTask(RegistrationActivity.this, WebUrls.BASE_URL + WebUrls.Api_Signup, objectNew, RegistrationActivity.this, RequestCode.CODE_Register, 1);
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_Register) {
            try {
                JSONObject object = new JSONObject(response);
                JSONObject innerObj = new JSONObject(object.getString("success"));
                JSONObject innerObjMsg = new JSONObject(innerObj.getString("msg"));
                String SuccessMsg = innerObjMsg.getString("replyMessage");
                JSONObject innerObj1 = new JSONObject(innerObj.getString("data"));
                JSONObject otp_obj = innerObj1.optJSONObject("signupOtp");
                startActivity(new Intent(RegistrationActivity.this, OtpActivity.class).putExtra("id", innerObj1.optString("id")).putExtra("otp", otp_obj.optInt("otp") + ""));
                finish();

            } catch (Exception e) {

            }
        }

    }
}
