package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.Service.GoogleService;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.util.DirectionsJSONParser;
import com.bababazri.advosoft.bababajriMTDNew.util.ParserTask;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.DriverRequestWrapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import in.shadowfax.proswipebutton.ProSwipeButton;

/**
 * Created by advosoft on 6/25/2018.
 */

public class DriverHomeActivity extends BaseActiivty implements OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, ParserTask.ParserCallback, WebCompleteTask {
    ArrayList markerPoints = new ArrayList();
    LatLng source = new LatLng(0.0, 0.0);
    LatLng destination = new LatLng(0.0, 0.0);
    String sourceAddress, destinationAddress;
    Polyline polyline;
    Polyline polyline_refresh;
    int counter = 2;
    Marker sourceMarker, carMarker;
    List<HashMap<String, String>> routePoints;
    List<HashMap<String, String>> refreshPoints;
    @Bind(R.id.swipe_start)
    ProSwipeButton swipeStart;
    @Bind(R.id.swipe_end)
    ProSwipeButton swipeEnd;
    @Bind(R.id.navigation)
    ImageView navigation;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    private GoogleMap mMap;
    DriverRequestWrapper feed;
    static String trip_status = "start";
    private List<LatLng> polyLineList = new ArrayList<>();
    Handler handler;
    LatLng startPosition;
    LatLng endPosition;
    private int index, next;

    DatabaseReference ref;
    ValueEventListener val_listner;

    /* Runnable drawPathRunnable = new Runnable() {
         @Override
         public void run() {
             if (index < polyLineList.size()-1){
                 index++;
                 next = index + 1;
             }
             if (index < polyLineList.size()-1){
                 startPosition = polyLineList.get(index);
                 endPosition = polyLineList.get(next);
             }
             final ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1);
             valueAnimator.setDuration(2000);
             valueAnimator.setInterpolator(new LinearInterpolator());
             valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                 @Override
                 public void onAnimationUpdate(ValueAnimator valueAnimator1) {
                      float v;
                      double lat,lng;
                     v = valueAnimator.getAnimatedFraction();
                     lng = v*endPosition.longitude+(1-v)*startPosition.longitude;
                     lat = v*endPosition.latitude + (1-v)*startPosition.latitude;
                     LatLng newPos = new LatLng(lat,lng);
                     carMarker.setPosition(newPos);
                     carMarker.setAnchor(0.5f,0.5f);
                     carMarker.setRotation(getBearing(startPosition,newPos));
 //                   tmMap.moveCamera(CameraUpdateFactory.newCameraPosition(
 //                           new CameraPosition.Builder()
 //                                   .target(newPos)
 //                                   .zoom(15.5f)
 //                                   .build()
 //                   ));
                 }
             });
             valueAnimator.start();
             handler.postDelayed(this,3000);
         }
     };
 */
    BroadcastReceiver locationUpdated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location newLocation = intent.getParcelableExtra("location");

            String lat = String.valueOf(newLocation.getLatitude());
            String lng = String.valueOf(newLocation.getLongitude());


            // if (!MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT).equalsIgnoreCase(lat) ||
            //         !MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG).equalsIgnoreCase(lng)) {

            Location location = new Location("");
            location.setLatitude(Double.parseDouble(lat));
            location.setLongitude(Double.parseDouble(lng));


            if (TextUtils.isEmpty(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT))) {
                    /*location.setBearing(bearingBetweenLocations(new LatLng(model.getSource().getLocation().getLat(), (model.getSource().getLocation().getLng())),
                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));*/
                location.setBearing(bearingBetweenLocations(new LatLng(source.latitude, source.longitude),
                        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
            } else {
                location.setBearing(bearingBetweenLocations(new LatLng(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)), Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG))),
                        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
            }
            //  animateMarker(location, carMarker);
            // animateMarkerNew(location, carMarker);
            MyApplication.setSharedPrefString(Constants.SP_CURRENT_LAT, lat);
            MyApplication.setSharedPrefString(Constants.SP_CURRENT_LONG, lng);

           /* DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Driver").child(MyApplication.getUserID(Constants.UserID));

            Long tsLong = System.currentTimeMillis() / 1000;
            Map<String, Object> jsonParams = new ArrayMap<>();
            jsonParams.put("lat", newLocation.getLatitude());
            jsonParams.put("lng", newLocation.getLongitude());
            jsonParams.put("timestamp", tsLong);
            mDatabase.updateChildren(jsonParams);
            Log.d("change_loc", trip_status);*/

               /* final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                if (trip_status.compareTo("refresh") == 0) {
                    new CountDownTimer(30000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            //  mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                            Log.d("timer", millisUntilFinished / 1000 + "");
                            getRoute(latLng);
                        }

                        public void onFinish() {

                        }
                    }.start();


                }*/


            //  }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_trip);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        feed = (DriverRequestWrapper) getIntent().getSerializableExtra("data");
        if (feed.getDeliveryStatus().compareTo("start") == 0) {
            swipeEnd.setVisibility(View.VISIBLE);
            swipeStart.setVisibility(View.GONE);
        } else {
            swipeEnd.setVisibility(View.GONE);
            swipeStart.setVisibility(View.VISIBLE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(DriverHomeActivity.this, GoogleService.class).putExtra("id", feed.getId()).putExtra("finish", false));

        } else {
            startService(new Intent(DriverHomeActivity.this, GoogleService.class).putExtra("id", feed.getId()).putExtra("finish", false));

        }
        checkGps();
        try {
            JSONObject src_obj = new JSONObject(feed.getPickupLocation());
            JSONObject des_obj = new JSONObject(feed.getCustomerLoaction());
            source = new LatLng(src_obj.optDouble("lat"), src_obj.optDouble("lng"));
            destination = new LatLng(des_obj.optDouble("lat"), des_obj.optDouble("lng"));
        } catch (Exception e) {

        }
        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // JSONObject des_obj = new JSONObject(feeds.getCustomerLoaction());
                    double lat = destination.latitude;
                    double lng = destination.longitude;
                    // String format = "geo:0,0?q=" + lat + "," + lng + "( Location title)";
                    String format = "google.navigation:q=" + lat + "," + lng;

                    Uri uri = Uri.parse(format);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setPackage("com.google.android.apps.maps");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } catch (Exception e) {

                }
            }
        });
        setMap();
        getRoute(source);

        swipeStart.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DriverHomeActivity.this);
                alertDialogBuilder.setMessage(R.string.is_good_lodded);
                alertDialogBuilder.setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                MyApplication.setSharedPrefString(Constants.SP_DriverStatus, "start");
                                swipeEnd.setVisibility(View.VISIBLE);
                                swipeStart.setVisibility(View.GONE);
                                trip_status = "refresh";
                                callapiAll("start");
                            }
                        });
                alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });


                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });
        Log.d("tripIdstatus", feed.getId());

        swipeEnd.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                MyApplication.setSharedPrefString(Constants.SP_DriverStatus, "end");
                swipeEnd.setVisibility(View.GONE);
                swipeStart.setVisibility(View.GONE);

                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations").child(feed.getId());
                Long tsLong = System.currentTimeMillis() / 1000;
                Map<String, Object> jsonParams = new ArrayMap<>();
                jsonParams.put("currntLatt", MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT));
                jsonParams.put("currntLong", MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG));
                jsonParams.put("jobStatus", "end");
                jsonParams.put("timestamp", tsLong);
                mDatabase.updateChildren(jsonParams);

                callapiAll("end");
            }
        });

        /*get from firebase*/
        ref = FirebaseDatabase.getInstance().getReference().child("Locations").child(feed.getId());
        val_listner = ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    try {
                        Map<String, String> value1 = (Map<String, String>) dataSnapshot.getValue();
                        Log.i("dataSnapshot json", "dataSnapshot json" + new JSONObject(value1));
                        JSONObject jsonObject = new JSONObject(value1);
                        if (jsonObject.optString("jobStatus").compareTo("pending") == 0) {
                            swipeEnd.setVisibility(View.GONE);
                            swipeStart.setVisibility(View.VISIBLE);

                        } else if (jsonObject.optString("jobStatus").compareTo("start") == 0) {
                            swipeEnd.setVisibility(View.VISIBLE);
                            swipeStart.setVisibility(View.GONE);

                        } else if (jsonObject.optString("jobStatus").compareTo("end") == 0) {
                            swipeEnd.setVisibility(View.GONE);
                            swipeStart.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        Log.d("exception...", e.toString());
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void callapiAll(String status) {
        HashMap objectNew = new HashMap();
        objectNew.put("tripStatus", status);
        objectNew.put("requestId", feed.getId());
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(DriverHomeActivity.this, WebUrls.BASE_URL + WebUrls.Api_DriverStatus, objectNew, DriverHomeActivity.this, RequestCode.CODE_DriverStatus, 1);
    }

    private void setMap() {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();

        getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        mapFragment.getMapAsync(DriverHomeActivity.this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }

        // Adding new item to the ArrayList
        markerPoints.add(source);
        markerPoints.add(destination);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(source);
        options.position(destination);

        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }


        sourceMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));
        /*carMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_truck))
                .title(sourceAddress));*/

        mMap.addMarker(new MarkerOptions().position(destination)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(destinationAddress));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        LocalBroadcastManager.getInstance(this).registerReceiver(locationUpdated, new IntentFilter("locationUpdated"));
    }

    @Override
    public void onMapLoaded() {

    }

    @Override
    public void onParserResult(List<List<HashMap<String, String>>> result, DirectionsJSONParser parser, String trip_status) {
        for (int i = 0; i < parser.getRouteList().size(); i++) {
            /*if (parser.getRouteList().get(i).getSelectedRoute().equalsIgnoreCase(model.getSelectedRoute())) {
                routePoints = result.get(i);
                showRoute(routePoints);
            }*/
            routePoints = result.get(i);
            Log.d("routePoints", routePoints + "");
            showRoute(routePoints, trip_status);
           /* if (trip_status.compareTo("start") == 0) {
                routePoints = result.get(i);
                Log.d("routePoints", routePoints + "");
                showRoute(routePoints, trip_status);
            } else {
                refreshPoints = result.get(i);
                Log.d("routePoints", refreshPoints + "");
                showRoute(refreshPoints, trip_status);
            }*/

        }


    }


    void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 0.5f);
            valueAnimator.setDuration(1000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));

                    } catch (Exception ex) {
                    }
                }

            });
            valueAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18.0f));
//                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18.0f));
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            valueAnimator.start();
            counter++;
        }
    }

    float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }


    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    private float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        Double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng.floatValue();
    }

    private void getRoute(LatLng src) {
        String url = MyApplication.getDirectionsUrl(src, destination);

        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(this, url, objectNew, DriverHomeActivity.this, RequestCode.CODE_MyVehical, 4);

       /* ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                ParserTask parserTask = new ParserTask(this);
                parserTask.execute(response.toString());
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });*/
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response", response);
        if (taskcode == RequestCode.CODE_MyVehical) {
            if (response != null) {

                ParserTask parserTask = new ParserTask(this, trip_status);
                parserTask.execute(response.toString());
            }
        } else if (taskcode == RequestCode.CODE_DriverStatus) {
            try {
                JSONObject object = new JSONObject(response);

              //  Toast.makeText(DriverHomeActivity.this, object.getJSONObject("success").getJSONObject("msg").optString("replyMessage"), Toast.LENGTH_SHORT).show();
                if (object.getJSONObject("success").getJSONObject("msg").optString("replyMessage").compareTo("Trip ended") == 0) {
                    Toast.makeText(DriverHomeActivity.this,getResources().getString(R.string.trip_end),Toast.LENGTH_SHORT).show();
                    MyApplication.setSharedPrefStringNew(Constants.SP_TripId, "");
                    MyApplication.setSharedPrefString(Constants.SP_DriverStatus, "");
                    ref.removeEventListener(val_listner);
                    startActivity(new Intent(DriverHomeActivity.this, DriverHome.class));
                    finish();


                } else {
                    MyApplication.setSharedPrefStringNew(Constants.SP_TripId, feed.getId());
                    Toast.makeText(DriverHomeActivity.this,getResources().getString(R.string.trip_start),Toast.LENGTH_SHORT).show();
                    try {
                        /*enter in firebase*/
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Locations").child(feed.getId());
                        Long tsLong = System.currentTimeMillis() / 1000;
                        Map<String, Object> jsonParams = new ArrayMap<>();
                        jsonParams.put("currntLatt", Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)));
                        jsonParams.put("currntLong", Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG)));
                        jsonParams.put("jobStatus", "start");
                        jsonParams.put("timestamp", tsLong);
                        mDatabase.updateChildren(jsonParams);

                        // JSONObject des_obj = new JSONObject(feeds.getCustomerLoaction());
                        double lat = destination.latitude;
                        double lng = destination.longitude;
                        // String format = "geo:0,0?q=" + lat + "," + lng + "( Location title)";
                        String format = "google.navigation:q=" + lat + "," + lng;

                        Uri uri = Uri.parse(format);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("com.google.android.apps.maps");
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } catch (Exception e) {

                    }
                }

            } catch (Exception e) {

            }

        }

    }

    private void showRoute(List<HashMap<String, String>> data, String trip_status) {
        if (polyline != null) {
            polyline.remove();
        }
        polyLineList.clear();
        ArrayList points = null;
        PolylineOptions lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        points = new ArrayList();
        lineOptions = new PolylineOptions();

        for (int j = 0; j < data.size(); j++) {
            HashMap<String, String> point = data.get(j);

            double lat = Double.parseDouble(point.get("lat").toString());
            double lng = Double.parseDouble(point.get("lng").toString());

            LatLng position = new LatLng(lat, lng);

            points.add(position);
            polyLineList.add(position);
//            builder.include(position);
        }

        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(getResources().getColor(R.color.colorPrimary));
        lineOptions.geodesic(true);
// Drawing polyline in the Google Map for the i-th route
        polyline = mMap.addPolyline(lineOptions);
//        LatLngBounds bounds = builder.build();

       /* handler = new Handler();
        index = -1;
        next = 1;
        handler.postDelayed(drawPathRunnable,3000);*/

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12);

       /* if (trip_status.compareTo("start") == 0) {
            if (polyline != null) {
                polyline.remove();
            }

            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
            points = new ArrayList();
            lineOptions = new PolylineOptions();

            for (int j = 0; j < data.size(); j++) {
                HashMap<String, String> point = data.get(j);

                double lat = Double.parseDouble(point.get("lat").toString());
                double lng = Double.parseDouble(point.get("lng").toString());

                LatLng position = new LatLng(lat, lng);

                points.add(position);
//            builder.include(position);
            }

            lineOptions.addAll(points);
            lineOptions.width(12);
            lineOptions.color(getResources().getColor(R.color.colorPrimary));
            lineOptions.geodesic(true);
// Drawing polyline in the Google Map for the i-th route
            polyline = mMap.addPolyline(lineOptions);
//        LatLngBounds bounds = builder.build();

            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.12);
        } else {
            if (polyline_refresh != null) {
                polyline_refresh.remove();
            }

            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
            points = new ArrayList();
            lineOptions = new PolylineOptions();

            for (int j = 0; j < data.size(); j++) {
                HashMap<String, String> point = data.get(j);

                double lat = Double.parseDouble(point.get("lat").toString());
                double lng = Double.parseDouble(point.get("lng").toString());

                LatLng position = new LatLng(lat, lng);

                points.add(position);
//            builder.include(position);
            }

            lineOptions.addAll(points);
            lineOptions.width(12);
            lineOptions.color(getResources().getColor(R.color.colorPrimary));
            lineOptions.geodesic(true);

// Drawing polyline in the Google Map for the i-th route
            polyline_refresh = mMap.addPolyline(lineOptions);

//        LatLngBounds bounds = builder.build();

            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.12);
        }*/


//        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//        mMap.animateCamera(cu);
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver_map, menu);
        return true;
    }*/

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_draw:
                //Toast.makeText(this, "You have selected Bookmark Menu", Toast.LENGTH_SHORT).show();
                LatLng latLng = new LatLng(Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LAT)), Double.parseDouble(MyApplication.getSharedPrefString(Constants.SP_CURRENT_LONG)));
                //getRoute(latLng);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(newPosition)
                                .zoom(15.5f)
                                .build()));

                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception ex) {
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    // if (mMarker != null) {
                    // mMarker.remove();
                    // }
                    // mMarker = googleMap.addMarker(new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car)));

                }
            });
            valueAnimator.start();
        }
    }


    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    //Method for finding bearing between two points
 /*   private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
*/
    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);
        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void checkGps() {
        try {
            LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
            Boolean isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            Boolean isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnable == false) {
                startActivity(new Intent(DriverHomeActivity.this, DialogActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        } catch (Exception e) {

        }

    }

    @Override
    protected void onRestart() {
        try {
            checkGps();
        } catch (Exception e) {

        }

        super.onRestart();
    }
}
