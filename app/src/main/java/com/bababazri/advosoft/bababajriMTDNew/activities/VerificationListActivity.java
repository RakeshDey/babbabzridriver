package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.adapter.VerificationListAdapter;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.RequestListWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 7/4/2018.
 */

public class VerificationListActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.empty_view)
    TextView emptyView;
    private static final int VERTICAL_ITEM_SPACE = 10;
    ArrayList<RequestListWrapper> feedsListall = new ArrayList();
    VerificationListAdapter adapterall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coman_layout);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.verification_list);
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;
        callapiAll();
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(VerificationListActivity.this, WebUrls.BASE_URL + WebUrls.Api_AgentRequest + "?status=pending", objectNew, VerificationListActivity.this, RequestCode.CODE_AgentRequest, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        swipeRefreshLayout.setRefreshing(false);
        try {
            Log.e("response", response);
            if (taskcode == RequestCode.CODE_AgentRequest) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    JSONArray data = null;
                    data = object.getJSONObject("success").getJSONArray("data");
                    JSONObject jobj = null;
                    feedsListall.clear();
                    for (int i = 0; i < data.length(); i++) {
                        RequestListWrapper contract = new RequestListWrapper();
                        try {
                            jobj = data.getJSONObject(i);
                            contract.setId(jobj.getString("id"));
                            contract.setBookingStatus(jobj.optString("deliveryStatus"));
                            contract.setDeliveryDate(jobj.optString("deliveryDate"));
                            contract.setOrderDate(jobj.optString("orderDate"));
                            contract.setOrderQuantity(jobj.optString("orderQuantity"));
                            contract.setUnit(jobj.optString("unit"));
                            contract.setPrice(jobj.optInt("price") + "");
                            contract.setOwnerId(jobj.optString("ownerId"));
                            contract.setProductId(jobj.optString("productId"));
                            contract.setDistance(jobj.optString("deliveryDistance"));
                            contract.setAddress(jobj.optJSONObject("customerAddress").optString("formattedAddress"));
                            contract.setLatlng(jobj.optJSONObject("customerAddress").optJSONObject("location") + "");
                            contract.setSrc_address(jobj.optJSONObject("pickupAddress").optString("formattedAddress"));
                            contract.setSrc_location(jobj.optJSONObject("pickupAddress").optJSONObject("location") + "");
                            contract.setVehical_nmbr(jobj.optJSONObject("vehicle").optString("truckNumber"));
                            contract.setVehical_type(jobj.optJSONObject("vehicle").optJSONObject("vehicleType").optString("name"));
                            contract.setProduct(jobj.optJSONObject("material").optString("name"));
                            contract.setProduct_cat(jobj.optJSONObject("materialType").optString("name"));
                            contract.setCus_name(jobj.optJSONObject("customer").optString("fullName"));
                            contract.setCus_number(jobj.optJSONObject("customer").optString("mobile"));
                            contract.setCus_address(jobj.optJSONObject("customer").optJSONObject("address").optString("address"));
                            contract.setDriver_name(jobj.optJSONObject("driver").optString("fullName"));
                            contract.setDriver_number(jobj.optJSONObject("driver").optString("mobile"));

                            feedsListall.add(contract);
                            emptyView.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    setList();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void setList() {
        System.out.println("service_list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new VerificationListAdapter(VerificationListActivity.this, feedsListall);
            recycleView.setAdapter(adapterall);


        }
    }

    @Override
    protected void onRestart() {
        callapiAll();
        super.onRestart();
    }
}
