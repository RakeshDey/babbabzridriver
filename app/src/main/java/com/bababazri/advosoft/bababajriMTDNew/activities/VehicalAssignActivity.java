package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.adapter.VehicalAssignAdapter;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.VehicalListWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/20/2018.
 */

public class VehicalAssignActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.empty_view)
    TextView emptyView;

    private static final int VERTICAL_ITEM_SPACE = 5;
    ArrayList<VehicalListWrapper> feedsListall = new ArrayList();
    VehicalAssignAdapter adapterall;
    String driverId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coman_layout);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.vehical_list);
        driverId=getIntent().getStringExtra("id");
        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;
        callapiAll();
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(VehicalAssignActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetMyVehical, objectNew, VehicalAssignActivity.this, RequestCode.CODE_MyVehical, 0);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("rtesponse", response);
        if (taskcode==RequestCode.CODE_MyVehical){
            swipeRefreshLayout.setRefreshing(false);
            try{
                JSONObject object=new JSONObject(response);
                JSONArray data_array=object.getJSONObject("success").getJSONArray("data");
                JSONObject data_obj=null;
                feedsListall.clear();
                for (int i=0;i<data_array.length();i++){
                    data_obj=data_array.getJSONObject(i);
                    VehicalListWrapper vehicalListWrapper=new VehicalListWrapper();
                    vehicalListWrapper.setId(data_obj.optString("id"));
                    vehicalListWrapper.setTruckPic("");
                    vehicalListWrapper.setVehicalNo(data_obj.optString("truckNumber"));
                    vehicalListWrapper.setRegNo(data_obj.optString("registrationNo"));
                    vehicalListWrapper.setOwnerid(data_obj.optString("ownerId"));
                    vehicalListWrapper.setApproveStatus(data_obj.optString("approveStatus"));
                    vehicalListWrapper.setVehicalType(data_obj.optJSONObject("vehicleType").optString("name"));
                    feedsListall.add(vehicalListWrapper);
                }
                setList();
            }catch (Exception e){

            }
        }

    }
    public void setList() {
        System.out.println("service_list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new VehicalAssignAdapter(VehicalAssignActivity.this, feedsListall,driverId);
            recycleView.setAdapter(adapterall);


        }
    }
}
