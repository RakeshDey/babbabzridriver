package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.adapter.ProductListAdapter;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.ProductWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/20/2018.
 */

public class ProductListActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.empty_view)
    TextView emptyView;

    private static final int VERTICAL_ITEM_SPACE = 25;
    ArrayList<ProductWrapper> feedsListall = new ArrayList();
    ProductListAdapter adapterall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coman_layout);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.product_list);

        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;
        callapiAll();
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(ProductListActivity.this, WebUrls.BASE_URL + WebUrls.Api_ProductList, objectNew, ProductListActivity.this, RequestCode.CODE_MyVehical, 0);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("rtesponse", response);
        if (taskcode == RequestCode.CODE_MyVehical) {
            swipeRefreshLayout.setRefreshing(false);
            try {
                JSONObject object = new JSONObject(response);
                JSONArray data_array = object.getJSONObject("success").getJSONArray("data");
                JSONObject data_obj = null;
                feedsListall.clear();
                for (int i = 0; i < data_array.length(); i++) {
                    data_obj = data_array.getJSONObject(i);
                    ProductWrapper vehicalListWrapper = new ProductWrapper();
                    vehicalListWrapper.setId(data_obj.optString("id"));
                    vehicalListWrapper.setMaterialName(data_obj.optJSONObject("material").optString("name"));
                    vehicalListWrapper.setMaterialSubcat(data_obj.optJSONObject("materialType").optString("name"));
                    vehicalListWrapper.setSubType(data_obj.optString("subType"));
                    vehicalListWrapper.setVenderName(data_obj.optString("name"));
                    vehicalListWrapper.setPrice(data_obj.optDouble("price") + "");
                    vehicalListWrapper.setProductStatus(data_obj.optString("status"));
                    vehicalListWrapper.setUnit(data_obj.optString("unit"));
                    vehicalListWrapper.setProductImg(data_obj.optString("image"));
                    JSONObject address = new JSONObject(data_obj.optString("address"));
                    vehicalListWrapper.setAddress(address.optString("formattedAddress"));
                    feedsListall.add(vehicalListWrapper);
                }
                setList();
            } catch (Exception e) {

            }
        }

    }

    public void setList() {
        System.out.println("service_list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new ProductListAdapter(ProductListActivity.this, feedsListall);
            recycleView.setAdapter(adapterall);
            adapterall.notifyDataSetChanged();


        }
    }

    @Override
    protected void onRestart() {
        callapiAll();
        super.onRestart();

    }

    public void ReloadData() {
        callapiAll();
    }
}
