package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/15/2018.
 */

public class SignupActivity1 extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.mobile_no)
    EditText mobileNo;
    @Bind(R.id.sp_login_as)
    Spinner spLoginAs;
    @Bind(R.id.ll)
    LinearLayout ll;
    @Bind(R.id.btn_submit)
    Button btnSubmit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_1);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.registration);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
           finish();
        }
        return super.onOptionsItemSelected(item);
    }
    private void callSubmitTask() {

        if (TextUtils.isEmpty(mobileNo.getText().toString())) {
            mobileNo.setError(getString(R.string.notempty));
            mobileNo.requestFocus();
        }
        else {
            JSONObject object = new JSONObject();
            try {
                object.put("method", "login");
                JSONObject data = new JSONObject();
                data.put("mobileNumber", mobileNo.getText().toString());
                data.put("ln", MyApplication.getlangID(Constants.LangId));
                object.put("data", data);


            } catch (Exception e) {

            }
            HashMap objectNew = new HashMap();
            objectNew.put("request", object.toString());
            new WebTask(SignupActivity1.this, WebUrls.BASE_URL, objectNew, SignupActivity1.this, RequestCode.CODE_Login, 1);
        }
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("response",response);

    }
}
