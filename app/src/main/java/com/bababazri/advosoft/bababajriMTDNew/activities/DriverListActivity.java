package com.bababazri.advosoft.bababajriMTDNew.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bababazri.advosoft.bababajriMTDNew.R;
import com.bababazri.advosoft.bababajriMTDNew.adapter.DriverListAdapter;
import com.bababazri.advosoft.bababajriMTDNew.fragments.MyApplication;
import com.bababazri.advosoft.bababajriMTDNew.views.Constants;
import com.bababazri.advosoft.bababajriMTDNew.webutility.RequestCode;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebCompleteTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebTask;
import com.bababazri.advosoft.bababajriMTDNew.webutility.WebUrls;
import com.bababazri.advosoft.bababajriMTDNew.wrapper.DriverListWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by advosoft on 6/20/2018.
 */

public class DriverListActivity extends BaseActiivty implements WebCompleteTask {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.app_bar)
    AppBarLayout appBar;
    @Bind(R.id.recycleView)
    RecyclerView recycleView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.empty_view)
    TextView emptyView;

    private static final int VERTICAL_ITEM_SPACE = 5;
    ArrayList<DriverListWrapper> feedsListall = new ArrayList();
    DriverListAdapter adapterall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coman_layout);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.driver_list);

        recycleView.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(llm);
        recycleView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        callapiAll();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }

    boolean isRefreshing = false;

    public void refreshList() {
        isRefreshing = true;
        callapiAll();
    }

    private void callapiAll() {
        HashMap objectNew = new HashMap();
        Log.d("auth", MyApplication.getUserType(Constants.userType));

        new WebTask(DriverListActivity.this, WebUrls.BASE_URL + WebUrls.Api_GetMyDriver, objectNew, DriverListActivity.this, RequestCode.CODE_MyVehical, 0);
    }

    @Override
    public void onComplete(String response, int taskcode) {
        Log.d("rtesponse", response);
        if (taskcode == RequestCode.CODE_MyVehical) {
            swipeRefreshLayout.setRefreshing(false);
            try {
                JSONObject object = new JSONObject(response);
                JSONArray data_array = object.getJSONObject("success").getJSONArray("data");
                JSONObject data_obj = null;
                feedsListall.clear();
                for (int i = 0; i < data_array.length(); i++) {
                    data_obj = data_array.getJSONObject(i);
                    DriverListWrapper vehicalListWrapper = new DriverListWrapper();
                    vehicalListWrapper.setId(data_obj.optString("id"));
                    vehicalListWrapper.setBlock(data_obj.optBoolean("isBlock"));
                    vehicalListWrapper.setProfile(data_obj.optString("profileImage"));
                    vehicalListWrapper.setName(data_obj.optString("firstName") + " " + data_obj.optString("lastName"));
                    vehicalListWrapper.setNumber(data_obj.optString("mobile"));
                    vehicalListWrapper.setFristName(data_obj.optString("firstName"));
                    vehicalListWrapper.setLastName(data_obj.optString("lastName"));
                    vehicalListWrapper.setDriverLicense(data_obj.optString("driverLicense"));
                    vehicalListWrapper.setPoliceVerify(data_obj.optString("policeVerify"));
                    vehicalListWrapper.setAadharCard(data_obj.optString("aadharCard"));
                    vehicalListWrapper.setImage(data_obj.optString("image"));
                    JSONObject address = new JSONObject(data_obj.optString("address"));
                    vehicalListWrapper.setAddress(address.optString("address"));
                    feedsListall.add(vehicalListWrapper);
                }
                setList();
            } catch (Exception e) {

            }
        }

    }

    public void setList() {
        System.out.println("service_list" + feedsListall.size());
        if (feedsListall.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            adapterall = new DriverListAdapter(DriverListActivity.this, feedsListall);
            recycleView.setAdapter(adapterall);
            adapterall.notifyDataSetChanged();


        }
    }

    public void Noti(){
       callapiAll();

    }
}
